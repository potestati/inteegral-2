<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oblast extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'areaName', 
        'poslovnaOblast', 
        'slug',
        'status',
        'category_id',
    ];

    public function availableProjects(){
        return $this->hasMany('App\AvailableProjects');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }
}
