<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upit extends Model {
//ovo ispod sam dodala pitaj da li treba
    //Table Name
    public $table = "upiti";
    // Primary Key - ovo sam dodala da li je tako ok ili treba bez ovoga da je id primaryKey
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'projectName',
        'projectValue',
        'projectDescription',
        'companyDescr',
        'regionDescr',
        'projectManager',
        'legalRegulations',
        'projectPotencial',
        'contactData',
        'businessArea',
        'scaleValue',
        'project_id'
    ];

    public function project() {
        return $this->belongsTo('App\Project');
    }

}
