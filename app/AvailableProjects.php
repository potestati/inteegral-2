<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableProjects extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'projectName', 'status', 'oblasti_id','amount', 'slug'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function oblast()
    {
        return $this->belongsTo('App\Oblast');
    }
}
