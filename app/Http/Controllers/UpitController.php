<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AvailableProjects;
use App\Project;
use App\Upit;
use DB;

class UpitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function addUpit(Request $request){
        //die and dump
		// dd($request->all());
        // return back();

		$this->validator($request);

		Upit::create([
			'projectName'=> $request->projectName, 
			'projectValue'=> $request->projectValue,
			'projectDescription'=> $request->projectDescription,
			'companyDescr'=> $request->companyDescr,
			'regionDescr'=> $request->regionDescr, 
			'projectManager'=> $request->projectManager,
			'legalRegulations'=> $request->legalRegulations,
			'projectPotencial'=> $request->projectPotencial, 
			'contactData'=> $request->contactData,
			'businessArea'=> $request->businessArea,
            'scaleValue'=> $request->scaleValue,
            'project_id' => $request->projectId,
		]);


		return redirect('/home')->with('success', 'Vasa registracija je kreirana, uskoro ce Vam se javiti administrator');
    }
    
    protected function validator($request)
    {
        $request->validate([
            'projectName' =>'required',
            'projectValue' => 'required',
            'projectDescription' => 'required',
            'companyDescr' => 'required',
            'regionDescr' => 'required',
            'projectManager' => 'required',
            'legalRegulations' => 'required',
            'projectPotencial' => 'required',
            'contactData' => 'required',
            'businessArea' => 'required',
            'scaleValue' => 'required',
        ]);
    }



    public function index()
    {
        //$availableProjects = AvailableProjects::all();
        //u view index.blade iz foldera availableProjects , prosledjujemo sve
        //raspolozive projekte iz baze iz tabele availableProjects u varijablu $availableProjects, nju posle pozivamo
        //u view
        //$availableProjects = AvailableProjects::orderBy('title', 'desc')->get();
        //$availableProjects = AvailableProjects::orderBy('title', 'desc')->take(1)->get();
        //get title from only availableProjects two
        //$availableProject = AvailableProjects::where('title', 'AvailableProjects Two')->get();
        //$availableProject = DB::select('SELECT * FROM available_projects');
        $availableProjects = AvailableProjects::orderBy('created_at', 'desc')->paginate(2);

        return view('availableProjects.index')->with('available_projects', $availableProjects);
    }

 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showAvailableProjectsDashboard(){
        return view('availableProjects.adminAvailable');
    }
     //Store funkcija je za validaciju i unos u bazu podataka
    public function create(Request $request)
    {
        //$name=$request->username; 
        //$role = App\Role::find(2);
        //ovo gore je za unos u bazu iz forme iz svakog input polja

        // $this->validate($request, [
        //     'projectName' => 'required',
        //     'status' => 'required',
        //     'category' => 'required',
        //     'amount' => 'required',

        // ]);
        
        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        
        //Create Available Projects
        $availableProject = new AvailableProjects();
        
        $availableProject->projectName = $request->projectName;
        $availableProject->slug = $request->slug;
        $availableProject->status = $request->status;
        $availableProject->category_id = $request->category_id;
     
        $availableProject->save();

        return redirect('/dashboard')->with('success', 'Available Projects Created');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $availableProject = AvailableProjects::find($id);
        return view('availableProjects.show')->with('availableProject', $availableProjects);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit Available Projects
        $availableProject = AvailableProjects::find($id);
        //NIJE MI JASNO STA SU ARGUMENTI U WITH
        return view('availableProjects.edit')->with('availableProject', $availableProject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //edit Available Projects
        $this->validate($request, [
            'projectName' => 'required',
            'status' => 'required',
            'category' => 'required',
            'amount' => 'required',

        ]);
        
        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        
        //Create Available Projects
        $availableProject = AvailableProjects::find($id);
        $availableProject->projectName = $request->input('projectName');
        $availableProject->status = $request->input('status');
        $availableProject->category = $request->input('category');
        $availableProject->amount = $request->input('amount');
        //$availableProject->body = $request->input('body');
        $availableProject->save();

        return redirect('/availableProjects')->with('success', 'Available Projects Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $availableProject = AvailableProjects::find($id);
        $availableProject->delete();
        return redirect('/availableProjects')->with('success', 'Available Projects Removed');

    }
}
