<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use App\Category;
use App\Upit;
use Auth;
use App\User;
use App\Role;
use DB; 

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     //svaki metod koji prodje kroz DashboardController ce biti kontrolisan od strane middleware
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->isAdmin()){
            //ovde dobijam project tabelu i samo dodajem podatke iz user i category tabele
            $projects = Project::with('user')->with('category')->get();
            return view('dashboard')->with('projects', $projects);
        }else{
            return redirect('/');
        }
    }

    public function singleApplication($id){
        $projects = Project::with('user')->with('category')->get();
               
        return view('projectReview')->with('singleApplication', $projects);
    }

    public function showprojectReview($id){
        $project = Project::findOrFail($id);
        //$project->category_id;
        return view('projectReview')->with('singleProject',  $project);
    }

    public function addStatus(Request $request){
        $projectsAll = $request->all();
        // echo "<pre>";
          foreach($projectsAll as $key => $status){
            if($key != '_token'){
                $projectId = explode("_", $key )[1];
                //var_dump($projectStatus);
                DB::table('project')->where('id', $projectId)->update(['status'=> $status]);

            }
        }
        // echo "</pre>";
        // $project_id = $request->id;
        // $status = $request->status;
        // DB::table('project')->where('id', $project_id)->update(['status'=> $status]);

        return redirect()->back();
    }

}
