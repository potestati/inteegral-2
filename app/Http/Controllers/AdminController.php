<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use App\Category;
use App\Upit;
use Auth;
use App\User;
use App\Admin;
use App\Role;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     //svaki metod koji prodje kroz DashboardController ce biti kontrolisan od strane middleware
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //ovde dobijam project tabelu i samo dodajem podatke iz user i category tabele
        $projects = Project::with('user')->with('category')->get();
               
        return view('dashboard')->with('projects', $projects);
    }

    public function singleApplication($id){
        $projects = Project::with('user')->with('category')->get();
               
        return view('projectReview')->with('singleApplication', $projects);
    }

    public function showprojectReview($id){
        $project = Project::findOrFail($id);
        //singleProject ce posle biti varijabla u blade koji pozivamo $singleProject
        return view('projectReview')->with('singleProject',  $project);
    }

 
}
