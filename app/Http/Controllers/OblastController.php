<?php

namespace App\Http\Controllers;

use App\Oblast;
use App\Category;
use App\AvailableProjects;
use App\Project;
use App\Upit;
use Illuminate\Http\Request;

class OblastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $category = Category::where('slug', $catName)->first();
    
        $oblasti = $category->areaName;
    
        //$paket['availableProjects'] = $category->availableProjects;
        //$paket['drugiPodatak'] = 'drugi podatak';
        //pozvana je metoda definisana u Category modelu availableProjects ne treba ()
        // return view('availableProjects.available')->with('availableProjects', $availableProjects);
        return view('oblasti.oblasti')->with('oblasti', $oblasti);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Oblast  $oblast
     * @return \Illuminate\Http\Response
     */
    public function show(Oblast $oblast)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Oblast  $oblast
     * @return \Illuminate\Http\Response
     */
    public function edit(Oblast $oblast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Oblast  $oblast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oblast $oblast)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oblast  $oblast
     * @return \Illuminate\Http\Response
     */
    public function destroy(Oblast $oblast)
    {
        //
    }
}
