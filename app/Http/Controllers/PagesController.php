<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\AvailableProjects;
use App\Project;
use App\Upit;

class PagesController extends Controller
{
    public function index()
    {
        //ova funkcija ce da potrazi pages folder i index blade fajl
        $title = 'INTEGRAL : GIM Zrenjanin';
        //return view('pages.index', compact('title'));
        return view('pages.index')->with('title', $title);
    }
    /* kreiranje home page koja je welcome page nakon registracije ispisuje kategorije blade home */
    public function home()
    {
        $categories = Category::all();
        return view('home')->with('categories', $categories);
    }
    /* kreiranje about page */
    public function about()
    {
        $title = 'ПРОФИЛ ФИРМЕ';
        return view('pages.about')->with('title', $title);
    }
    /* kreiranje services page */
    public function services()
    {
        $data =  array(
            'title' => 'Services',
            'services' => ['Web Design', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }

    /* available projects  */
    public function availableProjects($catName){
        $category = Category::where('slug', $catName)->first();

        $availableProjects = $category->availableProjects;

        $paket['availableProjects'] = $category->availableProjects;
        $paket['drugiPodatak'] = 'drugi podatak';
        //pozvana je metoda definisana u Category modelu availableProjects ne treba ()
        // return view('availableProjects.available')->with('availableProjects', $availableProjects);
        return view('availableProjects.available')->with('paket', $paket);
    }

    public function project($projectSlug){
        //echo $projectSlug;
        $availableProject = AvailableProjects::where('slug','=', $projectSlug)->first();
        $paket2['availableProject'] = AvailableProjects::where('slug','=', $projectSlug)->first();
        $paket2['cat'] = $availableProject['category_id'];
        // ovo mora da prosledi podatak o projectu (iz sluga verovatno)
        return view('projects.project')->with('paket2', $paket2);
    }
    public function showUpit($projectName, $projectId){
        $upit = Upit::where('projectName','=', $projectName)->first();
        return view('upiti.upit')->with('projectId', $projectId);
    }
}
