<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upit;
use App\Oblast;
use App\Project;
use App\AvailableProjects;
use DB;

class CategoriesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //$categories = Category::all();
        //u view index.blade iz foldera upiti , prosledjujemo sve
        //upite iz baze iz tabele upiti u varijablu $upiti, nju posle pozivamo
        //u view
        //$category = Upit::orderBy('title', 'desc')->get();
        //$category = Upit::orderBy('title', 'desc')->take(1)->get();
        //get title from only upit two
        //$category = Upit::where('title', 'Upit Two')->get();
        //$category = DB::select('SELECT * FROM categories');
        $categories = Category::orderBy('created_at', 'desc')->paginate(2);

        return view('categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Store funkcija je za validaciju i unos u bazu podataka
    public function store(Request $request) {
        //$name=$request->username; 
        //$role = App\Role::find(2);
        //ovo gore je za unos u bazu iz forme iz svakog input polja

        $this->validate($request, [
            'categoryName'=> 'required',
            'categoryValue'=> 'required',
            'categoryDescription'=> 'required',
            'categoryManager'=> 'required',
            'legalRegulations'=> 'required',
            'categoryPotencial'=> 'required',
            'contactData'=> 'required',
            'businessArea'=> 'required',
            'scaleValue'=> 'required',
        ]);

        $cateogry = new Category;
        $cateogry->categoryName = $request->input('categoryName');
        $cateogry->categoryValue = $request->input('categoryValue');
        $cateogry->categoryDescription = $request->input('categoryDescription');
        $cateogry->categoryManager = $request->input('categoryManager');
        $cateogry->legalRegulations = $request->input('legalRegulations');
        $cateogry->categoryPotencial = $request->input('categoryPotencial');
        $cateogry->contactData = $request->input('contactData');
        $cateogry->businessArea = $request->input('businessArea');
        $cateogry->scaleValue = $request->input('scaleValue');
        //$cateogry->body = $request->input('body');
        $cateogry->save();

        return redirect('/categories')->with('success', 'Category Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $category = Category::find($id);
        return view('categories.show')->with('categories', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //edit upit
        $category = Category::find($id);
        return view('categories.edit')->with('categories', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //edit upit
        $this->validate($request, [
            'categoryName'=> 'required',
            'categoryValue'=> 'required',
            'categoryDescription'=> 'required',
            'categoryManager'=> 'required',
            'legalRegulations'=> 'required',
            'categoryPotencial'=> 'required',
            'contactData'=> 'required',
            'businessArea'=> 'required',
            'scaleValue'=> 'required',
            //'body' => 'required'
        ]);

        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        //Create Upit

        $category = Category::find($id);
        $cateogry->categoryName = $request->input('categoryName');
        $cateogry->categoryValue = $request->input('categoryValue');
        $cateogry->categoryDescription = $request->input('categoryDescription');
        $cateogry->categoryManager = $request->input('categoryManager');
        $cateogry->legalRegulations = $request->input('legalRegulations');
        $cateogry->categoryPotencial = $request->input('categoryPotencial');
        $cateogry->contactData = $request->input('contactData');
        $cateogry->businessArea = $request->input('businessArea');
        $cateogry->scaleValue = $request->input('scaleValue');

        //$category->body = $request->input('body');
        $category->save();

        return redirect('/categories')->with('success', 'Category Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $category = Category::find($id);
        $category->delete();
        return redirect('/categories')->with('success', 'Category Removed');
    }

}
