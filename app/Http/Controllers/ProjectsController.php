<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use DB;

class ProjectsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        Project::all();
        // $projects = Project::all();
        //u view index.blade iz foldera projects , prosledjujemo sve
        //projekte iz baze iz tabele project u varijablu $projects, nju posle pozivamo
        //u view
        // $projects = Project::orderBy('projectName', 'projectSector')->get();
        // $projects = Project::orderBy('projectName', 'projectSector')->take(1)->get();
        //get title from only project two
        //$project = Project::where('title', 'Project Two')->get();
        //$project = DB::select('SELECT * FROM project');
        // $projects = Project::orderBy('created_at', 'desc')->paginate(2);

        // return view('projects.index')->with('project', $projects);
        return view('projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Store funkcija je za validaciju i unos u bazu podataka
    public function store(Request $request) {
        //$name=$request->username; 
        //$role = App\Role::find(2);
        //ovo gore je za unos u bazu iz forme iz svakog input polja

        $this->validate($request, [

            'projectName' => 'required',
            'projectSector' => 'required',
            'projectValue' => 'required',
            'selectedMap' => 'required',
            'projectManager' => 'required',
            'contactData' => 'required',
            'website' => 'required',
            'email' => 'required',
            'address' => 'required',
            'projectCharacter' => 'required',
            'regionCharacter' => 'required',
            'offeredCooperation' => 'required',
            'certificates' => 'required',
            'contactPerson' => 'required',
            // category treba da bi se unela od strane administratora
            'category' => 'required',

        ]);

        //Create Project
        $project = new Project;
        $project->projectName = $request->input('projectName');
        $project->projectSector = $request->input('projectSector');
        $project->projectValue = $request->input('projectValue');
        $project->selectedMap = $request->input('selectedMap');
        $project->projectManager = $request->input('projectManager');
        $project->contactData = $request->input('contactData');
        $project->website = $request->input('website');
        $project->email = $request->input('email');
        $project->address = $request->input('address');
        $project->projectDescription = $request->input('projectDescription');
        $project->projectCharacter = $request->input('projectCharacter');
        $project->regionCharacter = $request->input('regionCharacter');
        $project->offeredCooperation = $request->input('offeredCooperation');
        $project->certificates = $request->input('certificates');
        $project->contactPerson = $request->input('contactPerson');
        $project->category = $request->input('category');
        $project->save();

        return redirect('/projects')->with('success', 'Project Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $project = Project::find($id);
        return view('projects.show')->with('project', $project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //edit project
        $project = Project::find($id);
        return view('projects.edit')->with('project', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //edit project
        $this->validate($request, [

            'projectName' => 'required',
            'projectSector' => 'required',
            'projectValue' => 'required',
            'selectedMap' => 'required',
            'projectManager' => 'required',
            'contactData' => 'required',
            'website' => 'required',
            'email' => 'required',
            'address' => 'required',
            'projectCharacter' => 'required',
            'regionCharacter' => 'required',
            'offeredCooperation' => 'required',
            'certificates' => 'required',
            'contactPerson' => 'required',
            'category' => 'required',
        ]);

        //return 123 ovde treba da imamo proveru unosa i poruke kao akciju 
        //nakon submit, pa u folderu inc pravimo novi blade message.blade.php
        //Create Project

        $project = Project::find($id);
        $project->projectName = $request->input('projectName');
        $project->projectSector = $request->input('projectSector');
        $project->projectValue = $request->input('projectValue');
        $project->selectedMap = $request->input('selectedMap');
        $project->projectManager = $request->input('projectManager');
        $project->contactData = $request->input('contactData');
        $project->website = $request->input('website');
        $project->email = $request->input('email');
        $project->address = $request->input('address');
        $project->projectCharacter = $request->input('projectCharacter');
        $project->regionCharacter = $request->input('regionCharacter');
        $project->offeredCooperation = $request->input('offeredCooperation');
        $project->certificates = $request->input('certificates');
        $project->contactPerson = $request->input('contactPerson');
        $project->category = $request->input('category');
        $project->save();

        return redirect('/projects')->with('success', 'Project Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $project = Project::find($id);
        $project->delete();
        return redirect('/projects')->with('success', 'Project Removed');
    }

}
