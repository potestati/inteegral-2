<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Category;

class ProjectController extends Controller {

    public function addProject(Request $request) {
        
        $this->validator($request);

        $noviPodatak = $request->novoPolje;

        // $glupost = Category::where('projectName', $category_id)->first();
        $project = Project::create([
            'projectName' => $request->projectName,
            'projectSector' => $request->projectSector,
            'projectValue' => $request->projectValue,
            'selectedMap' => $request->selectedMap,
            'projectManager' => $request->projectManager,
            'contactData' => $request->contactData,
            'website' => $request->website,
            'email' => $request->email,
            'address' => $request->address,
            'projectDescription' => $request->projectDescription,
            'projectCharacter' => $request->projectCharacter,
            'regionCharacter' => $request->regionCharacter,
            'offeredCooperation' => $request->offeredCooperation,
            'certificates' => $request->certificates,
            'contactPerson' => $request->contactPerson,
            // 'user_id' => Auth::id(),
            'user_id' => Auth::user()->id,
            // 'category_id' => Category::id(),
            'category_id' => $request->prosledjeniCategoryID,
            'status' => 0,
        ]);

        return redirect()->route('upit', [
            'projectSlug' => $request->slug,
            'projectId' => $project->id
            ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($request)
    {
        $request->validate([
            'projectName' =>'required',
            'projectSector' => 'required',
            'projectValue' => 'required',
            'selectedMap' => 'required',
            'projectManager' => 'required',
            'contactData' => 'required',
            'website' => 'required',
            'email' => 'required',
            'address' => 'required',
            'projectDescription' => 'required',
            'projectCharacter' => 'required',
            'regionCharacter' => 'required',
            'offeredCooperation' => 'required',
            'certificates' => 'required',
            'contactPerson' => 'required',
        ]);
    }

    public function updateProject(Request $request, $id){
        //dd($request->all());
        Project::findOrFail($id)->update($request->all());
        return redirect('/dashboard');

    }

}
