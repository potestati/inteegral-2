<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {
    
    //Table Name
    public $table = "project";
    // Primary Key - ovo sam dodala da li je tako ok ili treba bez ovoga da je id primaryKey
    public $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'projectName',
        'projectSector',
        'projectValue',
        'selectedMap',
        'projectManager',
        'contactData',
        'website',
        'email',
        'address',
        'projectDescription',
        'projectCharacter',
        'regionCharacter',
        'offeredCooperation',
        'certificates',
        'contactPerson',
        'user_id',
        'status',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
