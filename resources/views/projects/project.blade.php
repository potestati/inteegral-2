@extends('layouts.dashboard')

@section('content')
<div style="display: none;">
{{$availableProject = $paket2['availableProject'] }}
</div>
<div class="container-fluid">
    <div class="container">

        <div class="formBox">
            <form method="POST" action="{{ route('addProject') }}" novalidate>
                <input name="prosledjeniCategoryID" id="prosledjeniCategoryID" type="hidden" value="{{$paket2['cat']}}">

                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <h1>ABOUT PROJECT / OPIS PROJEKTA</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox ">
                            <div class="inputText">{{ __('Project Name / Naziv projekta') }}</div>
                            <input id="projectName" type="text" class="input{{ $errors->has('projectName') ? ' is-invalid' : '' }}" name="projectName" value="{{ old('projectName') }}" required autofocus>

                            @if ($errors->has('projectName'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <br>
                <h2>Map of Investments – Mapa Investicija</h2>
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox ">
                            <div class="inputText">{{ __('Sector of Economy / Oblast') }}</div>
                            <input id="projectSector" type="text" class="input{{ $errors->has('projectSector') ? ' is-invalid' : '' }}" name="projectSector" value="{{ old('projectSector') }}" required autofocus>
                            @if ($errors->has('projectSector'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectSector') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Project value (EUR) / Vrednost') }}</div>
                            <input id="projectValue" type="text" class="input{{ $errors->has('projectValue') ? ' is-invalid' : '' }}" name="projectValue" value="{{ old('projectValue') }}" required autofocus>

                            @if ($errors->has('projectValue'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectValue') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('You apply with project as / Odabrana Mapa') }}</div>
                            <input id="selectedMap" type="text" class="input{{ $errors->has('selectedMap') ? ' is-invalid' : '' }}" name="selectedMap" value="{{ old('selectedMap') }}" required autofocus>

                            @if ($errors->has('selectedMap'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('selectedMap') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Project Manager / Funkcija') }}</div>
                            <input id="projectManager" type="text" class="input{{ $errors->has('projectManager') ? ' is-invalid' : '' }}" name="projectManager" value="{{ old('projectManager') }}" required autofocus>

                            @if ($errors->has('projectManager'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectManager') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Contact phone / Kontakt Tel: +381') }}</div>
                            <input id="contactData" type="text" class="input{{ $errors->has('contactData') ? ' is-invalid' : '' }}" name="contactData" value="{{ old('contactData') }}" required autofocus>

                            @if ($errors->has('contactData'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('contactData') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Website') }}</div>
                            <input id="website" type="text" class="input{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" value="{{ old('website') }}" required autofocus>

                            @if ($errors->has('website'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('website') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Email') }}</div>
                            <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Address / Adresa') }}</div>
                            <input id="address" type="text" class="input{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required autofocus>

                            @if ($errors->has('address'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Description / Opis projekta</div>
                            <br><br>
                            <textarea id="projectDescription" type="text" class="input{{ $errors->has('projectDescription') ? ' is-invalid' : '' }}" name="projectDescription" value="{{ old('projectDescription') }}" required autofocus></textarea>
                            @if ($errors->has('projectDescription'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectDescription') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Specify characteristics of the product / Specifične karakteristike proizvoda ili projekta ili patenta</div>
                            <br><br>
                            <textarea id="projectCharacter" type="text" class="input{{ $errors->has('projectCharacter') ? ' is-invalid' : '' }}" name="projectCharacter" value="{{ old('projectCharacter') }}" required autofocus></textarea>
                            @if ($errors->has('projectCharacter'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectCharacter') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Specify characteristics of the region and the benefits that the region may have from a realization of the project /<br> Specifične karakteristike regiona i koristi koje bi Grad – Opština dobila realizacijom ovog projekta</div>
                            <br><br>
                            <textarea id="regionCharacter" type="text" class="input{{ $errors->has('regionCharacter') ? ' is-invalid' : '' }}" name="regionCharacter" value="{{ old('regionCharacter') }}" required autofocus></textarea>
                            @if ($errors->has('regionCharacter'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('regionCharacter') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Cooperation offered to a foreign partner or investor /<br>Saradnja koju nudite stranom partner, odnosno investitoru</div>
                            <br><br>
                            <textarea id="offeredCooperation" type="text" class="input{{ $errors->has('offeredCooperation') ? ' is-invalid' : '' }}" name="offeredCooperation" value="{{ old('offeredCooperation') }}" required autofocus></textarea>
                            @if ($errors->has('offeredCooperation'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('offeredCooperation') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">Legal regulations (certificates, patent rights, land ownership, etc.)/<br>Pravni okvir i zakonodavna osnova predloga (sertifikati, patentna prava, vlasništvo nad zemljom, i drugo)</div>
                            <br><br>
                            <textarea id="certificates" type="text" class="input{{ $errors->has('certificates') ? ' is-invalid' : '' }}" name="certificates" value="{{ old('certificates') }}" required autofocus></textarea>
                            @if ($errors->has('certificates'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('certificates') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText"><i>Contact person/ Kontakt osoba</i></div>
                            <textarea id="contactPerson" type="text" class="input{{ $errors->has('contactPerson') ? ' is-invalid' : '' }}" name="contactPerson" value="{{ old('contactPerson') }}" required autofocus></textarea>
                            @if ($errors->has('contactPerson'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('contactPerson') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>

            <input type="hidden" name="slug" value="{{$availableProject->slug}}">
                
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" name="#" class="button" value="{{ __('Send Information / Pošalji Informacije') }}">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Start Script-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Main Script -->
{{--  <script src="js/main.js" type="text/javascript"></script>  --}}
<script type="text/javascript">
    $(".input").focus(function () {
        $(this).parent().addClass("focus");
    })
</script>
<!-- / Script-->

@endsection
