@extends('layouts.dashboard')

@section('content')
@include('inc.sidebar')
    <div class="col-sm-9">
        <div class="main">
 <!-- $projects}} -->
            <!-- Content Here -->
        <form method="POST" action="{{ route('addStatus') }}" novalidate>
                @csrf
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Aplikant</th>
                        <th scope="col">Kategorija</th>
                        <th scope="col">Naziv projekta</th>
                        <th scope="col">status</th>
                        <th scope="col">Izmeni projekat</th>
                        <th scope="col">Status projekta</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($projects as $index=>$project)
                        <tr>
                            <th scope="row">{{++$index}}</th>
                            <td>{{$project->user->fullName}}</td>
                            <td>{{$project->category->categoryName}}</td>
                            <td>{{$project->projectName}}</td>

                           
                            @if($project->status == 0)
                            <td>U procesu</td>
                            @elseif($project->status == 1)
                            <td>Odobren</td>
                            @elseif($project->status == 2)
                            <td>Odbijen</td>
                            @endif

                            <td><a href="{{route('projectReview', ['id'=> $project->id])}}" type="button" class="btn btn-primary" id="#">Izmeni projekat</a></td>
                            <td>
                            <select name="status_{{$project->id}}" class="form-control">
                            @if($project->status == 0)
                                <option value="0" selected="selected" >U procesu</option>
                                <option value="1">Odbijen</option>
                                <option value="2">Odobren</option>
                            @elseif($project->status == 1)
                                <option value="0">U procesu</option>
                                <option value="1"  selected="selected" >Odobren</option>
                                <option value="2">Odbijen</option>
                            @elseif($project->status == 2)
                                <option value="0">U procesu</option>
                                <option value="1">Odobren</option>
                                <option value="2" selected="selected">Odbijen</option>
                            @endif
                            </select>
                            </td>
                            <!-- <td><button class="btn btn-success" id="#">Odobri projekat</button></td> -->

                        </tr>  
                    @endforeach
                </tbody>
            </table>
            <input type="submit" value="Submit">
        </form>
        </div>
        </div>
        <!-- Start Script-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
       
        <!-- Popper JS -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        {{--  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>  --}}
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
        
@endsection
