@extends('layouts.app')

@section('content')
<style>
.row{
    float: left;
    width: 100%;
}
</style>

<div class="container-fluid show-upit">
    <div class="container">
        <h1 style="text-align: center;">{{ __('Poziv za Vaše učešće u projektu uspešnijeg pozicioniranja Grada ZRENJANINA') }}</h1>
        {{-- ovo stavi u poseban file na include--}}
        <p>Poštovani,</p>
        <p>Cilj projekta se na prvom mestu ogleda u efikasnijem i efektivnijem načinu privlačenja investicija – što znači da posetilac u najkraċem vremenskom periodu dođe upravo do onog projekta ili proizvoda koji ga interesuje u odnosu na oblast njegovog interesovanja i iznos potencijalnog ulaganja. Odnosno, ako konsultuje GIM radi kupovine određenih proizvoda ili ulaganja u određeni patent isti mu se odmah stavlja na uvid. </p>
        <p>Cilj je dostaviti klijentu tačnu, punu i kvalitetnu informaciju. </p>
        <p>Struktura projekta ogleda se u portalu koji otvara jednu geografsku mapu Grada/Opštine gde birate pod-mapu iz sledećih kategorija:</p>
        <ul>
            <li>MAPA INVESTICIONIH PROJEKATA : greenfield, brownfield, drugi</li>
            <li>MAPA STRATEŠKIH PARTNERSTAVA</li>
            <li>MAPA PROIZVODA I USLUGA atestiranih za izvoz, odnosno sertifikovanih za ponudu </li>
            <li>MAPA PATENATA.</li>
        </ul>
        <p>Klikom na željenu mapu otvaraju se dve kolone: </p>
        <ul>
            <li>jedna je vrednosna skala sa vrednostima potencijalnih ulaganja u kojima su projekti već selektovani i to za 50.000 €/ 200.000€/500.000€/1.000.000€/ 5.000.000€/preko 5 mil. €, </li>
            <li>do nje je druga kolona datih industrijskih, odnosno privrednih grana za koje postoje projekti u odnosu na mape. Time se u projektu postiže moć brzine pronalaska tražene informacije i delotvornosti promocije pravog projekta. Projekti su prikazani u uniformnim obrascima.</li>
        </ul>
        <p>Vas ljubazno molimo da:</p>
        <ul>
            <li>Odredite koordinatora u vašoj instituciji koji će sa nama koordinirati, odabirati projekte unutar vaše baze podataka, popuniti upit u odnosu na projekat, ostati u redovnom kontaktu sa nama</li>
        </ul>
        <p>Za sve dodatne informacije slobodno me možete kontaktirati putem telefona na 064 2776 963 ili putem maila na: president@teslianum.com</p>
        <p style="text-align: center;">S poštovanjem,</p>
        <div class="socket">
            <p style="text-align: left;">Mirjana Prljević<br>Autor projekta</p>
            <p style="text-align: right;">Miloš Dimitrijević<br>Koordinator projekta</p>
        </div>
        {{-- / ovo stavi u poseban file na include--}}
        <div class="formBox">
            <form  method="POST" action="{{ route('addUpit') }}" novalidate>
                @csrf
                <div class="row">
                    <br><br>
                    <div class="col-sm-12">
                        <h2 style="text-align: center;">{{ __('U P I T (služi za slanje internim grupama)') }}</h2>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('1.Naziv projekta (i svrstavanje u grupu – dato u prilogu)') }}</div>
                            <br><br>
                            <textarea id="projectName" type="text" class="form-control{{ $errors->has('projectName') ? ' is-invalid' : '' }}" name="projectName" value="{{ old('projectName') }}" required autofocus></textarea>

                            @if ($errors->has('projectName'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('2.Vrednost projekta (po skali -  dato na sledećoj strani)') }}</div>
                            <br><br>
                            <textarea id="projectValue" type="text" class="form-control{{ $errors->has('projectValue') ? ' is-invalid' : '' }}" name="projectValue" value="{{ old('projectValue') }}" required autofocus></textarea>

                            @if ($errors->has('projectValue'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectValue') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('3.Opis Projekta ') }}</div>
                            <br><br>
                            <textarea id="projectDescription" type="text" class="form-control{{ $errors->has('projectDescription') ? ' is-invalid' : '' }}" name="projectDescription" value="{{ old('projectDescription') }}" required autofocus></textarea>

                            @if ($errors->has('projectDescription'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('projectDescription') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('4.Opis Kompanije (datum osnivanja, broj zaposlenih, aktiva, poslovni bilansi, pozicija na tržištu razvojne perspektive,  
                                potencijalna izvozna orijentacija, poslovni partneri) ') }}</div>
                            <br><br>
                            <br><br>
                            <textarea id="companyDescr" type="text" class="form-control{{ $errors->has('companyDescr') ? ' is-invalid' : '' }}" name="companyDescr" value="{{ old('companyDescr') }}" required autofocus></textarea>

                            @if ($errors->has('companyDescr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('companyDescr') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText"{{ __('4.Opis Kompanije (datum osnivanja, broj zaposlenih, aktiva, poslovni bilansi, pozicija na tržištu razvojne perspektive,  
                            potencijalna izvozna orijentacija, poslovni partneri)') }}</div>
                                <br><br>
                                <textarea id="companyDescr" type="text" class="form-control{{ $errors->has('companyDescr') ? ' is-invalid' : '' }}" name="companyDescr" value="{{ old('companyDescr') }}" required autofocus></textarea>

                                @if ($errors->has('companyDescr'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('companyDescr') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('5.Opis regiona u kome se nalazi Projekat (za mape 1 i 2)') }}</div>
                            <br><br>
                            <textarea id="regionDescr" type="text" class="form-control{{ $errors->has('regionDescr') ? ' is-invalid' : '' }}" name="regionDescr" value="{{ old('regionDescr') }}" required autofocus></textarea>

                            @if ($errors->has('regionDescr'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('regionDescr') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="inputText">{{ __('6.Menadžer projekta') }}</div>
                                <br><br>
                                <textarea id="projectManager" type="text" class="form-control{{ $errors->has('projectManager') ? ' is-invalid' : '' }}" name="projectManager" value="{{ old('projectManager') }}" required autofocus></textarea>

                                @if ($errors->has('projectManager'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('projectManager') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('7.Pravna regulativa (sertifikati, atesti, zaštitita patentnog prava, zemljišno vlasništvo i dr.)') }}</div>
                                <br><br>
                                <textarea id="legalRegulations" type="text" class="form-control{{ $errors->has('legalRegulations') ? ' is-invalid' : '' }}" name="legalRegulations" value="{{ old('legalRegulations') }}" required autofocus></textarea>

                                @if ($errors->has('legalRegulations'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('legalRegulations') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('8.Potencijal projekta (šta se konkretno traži od strane predlagača projekta)') }}</div>
                                <br><br>
                                <textarea id="projectPotencial" type="text" class="form-control{{ $errors->has('projectPotencial') ? ' is-invalid' : '' }}" name="projectPotencial" value="{{ old('projectPotencial') }}" required autofocus></textarea>

                                @if ($errors->has('projectPotencial'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('projectPotencial') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="inputText">{{ __('9.Kontakt informacije (puna informacija koja postoji: web-site, mail, T/F, adresa)') }}</div>
                                <br><br>
                                <textarea id="contactData" type="text" class="form-control{{ $errors->has('contactData') ? ' is-invalid' : '' }}" name="contactData" value="{{ old('contactData') }}" required autofocus></textarea>

                                @if ($errors->has('contactData'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('contactData') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{--'businessArea', --}}
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="form-group">
                                    <label for="#">{{ __('OBLAST POSLOVANJA') }}</label>
                                    <select name="businessArea" multiple class="form-control" id="exampleFormControlSelect2">
                                        <option value="Avio industrija">Avio industrija </option>
                                        <option value="Automobilska industrija ">Automobilska industrija </option>
                                        <option value="Poslovne usluge">Poslovne usluge</option>
                                        <option value="Hemijska industrija">Hemijska industrija </option>
                                        <option value="Građevinska industrija">Građevinska industrija </option>
                                        <option value="Elektrotehnika i elektronika">Elektrotehnika i elektronika </option>
                                        <option value="Industrija hrane i pića, poljoprivreda">Industrija hrane i pića, poljoprivreda </option>
                                        <option value="Industrija kože">Industrija kože </option>
                                        <option value="Tekstilna industrij">Tekstilna industrija </option>
                                        <option value="Odeća">Odeća </option>
                                        <option value="Mašine i oprema">Mašine i oprema </option>
                                        <option value="Metalurgija i obrada metala">Metalurgija i obrada metala </option>
                                        <option value="Ambalaža">Ambalaža </option>
                                        <option value="Hartija">Hartija </option>
                                        <option value="Farmaceutska industrija">Farmaceutska industrija</option>
                                        <option value="Plastika i guma">Plastika i guma </option>
                                        <option value="Drvna industrija">Drvna industrija</option>
                                        <option value="Softver i informaciono-komunikacione tehnologije">Softver i informaciono-komunikacione tehnologije </option>
                                        <option value="Turistička ponuda">Turistička ponuda</option>
                                        <option value="Sportsko-rekreativna ponuda">Sportsko-rekreativna ponuda</option>
                                        <option value="Usluge loan-poslova">Usluge loan-poslova</option>
                                        <option value="Slobodne industrijske zone">Slobodne industrijske zone</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{--'scaleValue', --}}
                        <div class="col-sm-12">
                            <div class="inputBox">
                                <div class="form-group">
                                    <label for="#">{{ __('SKALA VREDNOSTI PROJEKTA') }}</label>
                                    <select name="scaleValue" class="form-control" id="exampleFormControlSelect1">
                                        <option value="Do 50.000">Do 50.000 &#8364;</option>
                                        <option value="Od 50.001–100.000">Od 50.001 – 100.000 &#8364;</option>
                                        <option value="Od 100.001–500.000">Od 100.001 – 500.000 &#8364;</option>
                                        <option value="Od 500.001–1.000.000 ">Od 500.001 – 1.000.000 &#8364;</option>
                                        <option value="Od 1 mil–5 mil">Od 1 mil – 5 mil &#8364;</option>
                                        <option value="Od 5 mil–10 mil">Od 5 mil – 10 mil &#8364;</option>
                                        <option value="Od 10 mil–50 mil">Od 50 mil &#8364; - 100 mil &#8364;</option>
                                        <option value="Od 50 mil-100 mil">Od 50 mil &#8364; - 100 mil &#8364;</option>
                                        <option value="Od 100 mil–500 mil">Od 100 mil – 500 mil &#8364;</option>
                                        <option value="Preko 500 mil">Preko 500 mil &#8364;</option>
                                    </select>
                                </div>
                                <div>
                                </div>
                            </div>
                            <div class="row">
                                <div  style="margin-bottom: 40px;" class="col-sm-12">
                                    <input type="submit" name="#" class="button" value="{{ __('Send Message / Posalji podatke)') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                <input type="hidden" value="{{$projectId}}" name="projectId">
                </form>
            </div>
        </div>
    </div>
    <!-- Start Script-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Main Script -->
    {{--  <script src="js/main.js" type="text/javascript"></script>  --}}
    <script type="text/javascript">
        $(".input").focus(function () {
            $(this).parent().addClass("focus");
        })
    </script>
    <!-- / Script-->
    @endsection
