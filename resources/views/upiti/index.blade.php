@extends('layouts.app')
@section('content')
<h1>Upiti</h1>
@if(count($upiti) > 0)
@foreach($upiti as $upit)
<div class="card card-body bg-light">
<h3><a href="/upiti/{{$upit->id}}">{{$upit->projectName}}</a></h3>

<small>Project Name : {{$upit->projectName}}</small>
<small>Project Value : {{$upit->projectValue}}</small>
<small>Project Description : {{$upit->projectDescription}}</small>
<small>Company Description : {{$upit->companyDescr}}</small>
<small>Region Description : {{$upit->regionDescr}}</small>
<small>Project Manager : {{$upit->projectManager}}</small>
<small>Legal Regulations : {{$upit->legalRegulations}}</small>
<small>Project Potencial : {{$upit->projectPotencial}}</small>
<small>Contact Data : {{$upit->contactData}}</small>
<small>Business Area : {{$upit->businessArea}}</small>
<small>Scale Value : {{$upit->scaleValue}}</small>
<small>Category : {{$upit->category}}</small>

</div>
@endforeach
<!-- ovo sluzi za paginaciju -->
{{$upiti->links()}}
@else
<p>There is no upita</p>
@endif
@endsection