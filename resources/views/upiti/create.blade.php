@extends('layouts.app')
@section('content')
<h1>Create Upit</h1>
{!! Form::open(['action' => 'UpitiController@store', 'method' => 'POST']) !!}
<div class="form-group">
	{{Form::label('projectName', 'Project Name')}}
	{{Form::text('projectName', '', ['class' => 'form-control', 'placeholder' => 'Project Name'])}}
</div>
<div class="form-group">
	{{Form::label('projectValue', 'Project Value')}}
	{{Form::text('projectValue', '', ['class' => 'form-control', 'placeholder' => 'Project Value'])}}
</div>
<div class="form-group">
	{{Form::label('projectDescription', 'Project Description')}}
	{{Form::text('projectDescription', '', ['class' => 'form-control', 'placeholder' => 'Project Description'])}}
</div>
<div class="form-group">
	{{Form::label('companyDescr', 'Company Descr')}}
	{{Form::text('companyDescr', '', ['class' => 'form-control', 'placeholder' => 'Company Descr'])}}
</div>
<div class="form-group">
	{{Form::label('regionDescr', 'Region Descr')}}
	{{Form::text('regionDescr', '', ['class' => 'form-control', 'placeholder' => 'Region Descr'])}}
</div>
<div class="form-group">
	{{Form::label('projectManager', 'Project Manager')}}
	{{Form::text('projectManager', '', ['class' => 'form-control', 'placeholder' => 'Project Manager'])}}
</div>
<div class="form-group">
	{{Form::label('legalRegulations', 'Legal Regulations')}}
	{{Form::text('legalRegulations', '', ['class' => 'form-control', 'placeholder' => 'Legal Regulations'])}}
</div>
<div class="form-group">
	{{Form::label('projectPotencial', 'Project Potencial')}}
	{{Form::text('projectPotencial', '', ['class' => 'form-control', 'placeholder' => 'Project Potencial'])}}
</div>
<div class="form-group">
	{{Form::label('contactData', 'Contact Data')}}
	{{Form::text('contactData', '', ['class' => 'form-control', 'placeholder' => 'Contact Data'])}}
</div>
<div class="form-group">
	{{Form::label('businessArea', 'Business Area')}}
	{{Form::text('businessArea', '', ['class' => 'form-control', 'placeholder' => 'Business Area'])}}
</div>
<div class="form-group">
	{{Form::label('scaleValue', 'Scale Value')}}
	{{Form::text('scaleValue', '', ['class' => 'form-control', 'placeholder' => 'Scale Value'])}}
</div>
<div class="form-group">
	{{Form::label('category', 'Category')}}
	{{Form::text('category', '', ['class' => 'form-control', 'placeholder' => 'Category'])}}
</div>
{{-- 
<div class="form-group">
	{{Form::label('body', 'Body')}}
	{{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
</div>
--}}
{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

{!! Form::close() !!}

@endsection