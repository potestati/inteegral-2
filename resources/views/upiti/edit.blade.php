@extends('layouts.app')
@section('content')
<h1>Edit Upit</h1>
{!! Form::open(['action' => ['UpitiController@update', $upit->id], 'method' => 'POST']) !!}
<div class="form-group">
    {{Form::label('projectName', 'Project Name')}}
    {{Form::text('projectName', $upit->title, ['class' => 'form-control', 'placeholder' => 'Project Name'])}}
</div>
<div class="form-group">
    {{Form::label('projectValue', 'Project Value')}}
    {{Form::text('projectValue', $upit->title, ['class' => 'form-control', 'placeholder' => 'Project Value'])}}
</div>
<div class="form-group">
    {{Form::label('projectDescription', 'Project Description')}}
    {{Form::text('projectDescription', $upit->title, ['class' => 'form-control', 'placeholder' => 'Project Description'])}}
</div>
<div class="form-group">
    {{Form::label('companyDescr', 'Company Descr')}}
    {{Form::text('companyDescr', $upit->title, ['class' => 'form-control', 'placeholder' => 'Company Descr'])}}
</div>
<div class="form-group">
    {{Form::label('regionDescr', 'Region Descr')}}
    {{Form::text('regionDescr', $upit->title, ['class' => 'form-control', 'placeholder' => 'Region Descr'])}}
</div>
<div class="form-group">
    {{Form::label('projectManager', 'Project Manager')}}
    {{Form::text('projectManager', $upit->title, ['class' => 'form-control', 'placeholder' => 'Project Manager'])}}
</div>
<div class="form-group">
    {{Form::label('legalRegulations', 'Legal Regulations')}}
    {{Form::text('legalRegulations', $upit->title, ['class' => 'form-control', 'placeholder' => 'Legal Regulations'])}}
</div>
<div class="form-group">
    {{Form::label('projectPotencial', 'Project Potencial')}}
    {{Form::text('projectPotencial', $upit->title, ['class' => 'form-control', 'placeholder' => 'Project Potencial'])}}
</div>
<div class="form-group">
    {{Form::label('contactData', 'Contact Data')}}
    {{Form::text('contactData', $upit->title, ['class' => 'form-control', 'placeholder' => 'Contact Data'])}}
</div>
<div class="form-group">
    {{Form::label('businessArea', 'Business Area')}}
    {{Form::text('businessArea', $upit->title, ['class' => 'form-control', 'placeholder' => 'Business Area'])}}
</div>
<div class="form-group">
    {{Form::label('scaleValue', 'Scale Value')}}
    {{Form::text('scaleValue', $upit->title, ['class' => 'form-control', 'placeholder' => 'Scale Value'])}}
</div>
<div class="form-group">
    {{Form::label('category', 'Category')}}
    {{Form::text('category', $upit->title, ['class' => 'form-control', 'placeholder' => 'Category'])}}
</div>
    {{-- 
    <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $upit->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
    </div>
    --}}
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

    {!! Form::close() !!}

    @endsection