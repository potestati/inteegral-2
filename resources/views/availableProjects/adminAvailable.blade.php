@extends('layouts.dashboard')

@section('content')

<div class="container-fluid">
        {{-- {{$availableProject->projectName}} --}}
    <h2>Ulogovani ste kao Administrator</h2>
    <br>
 
    <div class="container">
        <div class="formBox">
            <form method="POST" action="{{ route('createAvailableProjects') }}" novalidate>
                {{-- when there is the post request in form you need to put @csrf --}}
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <h1>ABOUT PROJECT / OPIS PROJEKTA</h1>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Project Name / Naziv projekta') }}</div>
                            <input id="projectName" type="text" class="input{{ $errors->has('projectName') ? ' is-invalid' : '' }}" name="projectName" value="{{ old('projectName') }}" required autofocus>

                            @if ($errors->has('projectName'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('projectName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="inputBox">
                            <div class="form-group">
                                <label for="#">{{ __('Kategorija projekta') }}</label>
                                <select name="category" multiple class="form-control" id="exampleFormControlSelect2">
                                    @foreach ($categories as $cat)
                                    <option value="{{$cat->id}}">{{$cat->categoryName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Amount') }}</div>
                            <input id="amount" type="text" class="input{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ old('amount') }}" required autofocus>

                            @if ($errors->has('amount'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
               
            {{-- <input type="hidden" name="slug" value="{{$availableProject->slug}}"> --}}
                
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" name="#" class="button" value="{{ __('Send Information / Pošalji Informacije') }}">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Start Script-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Main Script -->
{{--  <script src="js/main.js" type="text/javascript"></script>  --}}

<!-- / Script-->

@endsection
