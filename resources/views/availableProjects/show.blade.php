<!-- ovo je content za single availableProjects-->
@extends('layouts.app')
@section('content')
<a class="btn btn-info" href="/availableProjects">Go back</a>
<br><br>
<h1>{{$availableProject->projectName}}</h1>
<hr>
<small>Status of the project is {{$availableProject->status}}</small>
<hr>
<small>Category of the project is {{$availableProject->category}}</small>
<hr>
<small>Value of the project is {{$availableProject->amount}}</small>
{{-- 
<div>
    {{-- ako hocemo da ckeditor parsira html onda moramo staviti jednu viticastu i uzvicnike --}}
    {{--{!!$availableProject->body!!}
</div>
<small>Written on {{$availableProject->created_at}}</small>
<hr>
--}}
<a href="/availableProjects/{{$availableProject->id}}/edit" class="btn btn-info">Edit</a>
{{-- nakon submitovanja izmena akcija je na update funkciju u AvailableProjectsControlleru--}}
{!!Form::open(['action' => ['AvailableProjectsController@destroy' , $availableProject->id], 'method' => 'POST', 'class' => 'float-right'])!!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
{!! Form::close() !!}
@endsection