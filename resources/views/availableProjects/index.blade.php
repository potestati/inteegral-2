@extends('layouts.app')
@section('content')
<h1>Available Projects</h1>
@if(count($availableProjects) > 0)

<ul class="">
@foreach($availableProjects as $availableProject)
<li><a href="/availableProjects/{{$availableProject->id}}">{{$$availableProject->projectName}}</a>
<small>Status of the project is {{$availableProject->status}}</small>
<small>Category of the project is {{$availableProject->category}}</small>
<small>Value of the project is {{$availableProject->amount}}</small>
</li>
@endforeach
</ul>
<!-- ovo sluzi za paginaciju -->
{{$availableProjects->links()}}
@else
<p>There is no Available Projects</p>
@endif
@endsection

