@extends('layouts.modal')

@section('content')

<div style="display: none">
{{$availableProjects = $paket['availableProjects'] }}
</div>
<div class="available-projects">
<h2>Raspolozivi projekti za apliciranje</h2>
<hr>
@if(count($availableProjects) > 0)
<ul>
@foreach($availableProjects as $availableProject)
    <li style="{{$availableProject->status == 1 ? 'color: red' : 'color: gray'}}">
        <a href="/availableProjects/{{$availableProject->slug}}">{{$availableProject->projectName}}</a>
        <small>Status projekta je 
            @if($availableProject->status == 1)
            otvoren za konkurs
            @else
            trenutno zatvoren za konkurs
            @endif
        </small>
        <small>Projekat je u kategoriji {{$availableProject->category->categoryName}}</small>
        <hr>
    </li>
@endforeach
</ul>
@endif
</div>
<!-- Start Script-->
        <!-- Start Script-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <!-- Popper JS -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Main Script -->
<script type="text/javascript">
    $(".input").focus(function () {
        $(this).parent().addClass("focus");
    })
</script>
<!-- / Script-->

@endsection
