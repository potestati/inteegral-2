	@extends('layouts.app')

	@section('content')

    <br><br>
	@if(count($categories) > 0)
	<ul style="list-style: none;">
        @foreach($categories as $category)
            <li style="display: inline-block; float: left; width: 25%;" >
                <h4 style="text-align: center;">{{ $category->categoryName }}</h4>
                <!-- ovde se generise vrednost u href koja ce uci u varijablu catName koja je u rutama -->
                <a href="/category/{{$category->slug}}">
                    <img src="{{asset('img/'.$category->categoryImage)}}" alt="">
                </a>
            </li>
        @endforeach
	</ul>
	@endif


    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <!-- Main Script -->
    <script src="js/main.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <script type="text/javascript">
        $(".input").focus(function () {
        	$(this).parent().addClass("focus");
        })
    </script>
        <!-- / Script-->
    @endsection

