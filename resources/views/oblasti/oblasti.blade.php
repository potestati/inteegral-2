@extends('layouts.modal')

@section('content')

<div style="display: none">
<!--$availableProjects = $paket['availableProjects'] }}-->
</div>
<div class="available-projects">
<h2>Izaberite oblasti poslovanja / Please chose a Business Area</h2>
<br>
<h3>Map of Investment Projects and Scale of Values</h3>
<br>
<h3>Mapa investicionih projekata i skala vrednost</h3>
<hr>
@if(count($oblasti) > 0)
<ul>
@foreach($oblasti as $oblast)
    <li style="{{$oblast->status == 1 ? 'color: red' : 'color: gray'}}">
        <a href="/business-area/{{$oblast->slug}}">{{$oblast->areaName}}</a>
        <small>Status projekta je 
            @if($oblast->status == 1)
            otvoren za konkurs
            @else
            trenutno zatvoren za konkurs
            @endif
        </small>
        <small>Oblast je u kategoriji {{$oblast->category->categoryName}}</small>
        <hr>
    </li>
@endforeach
</ul>
@endif
</div>
<!-- Start Script-->
        <!-- Start Script-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <!-- Popper JS -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Main Script -->
<script type="text/javascript">
    $(".input").focus(function () {
        $(this).parent().addClass("focus");
    })
</script>
<!-- / Script-->

@endsection
