@extends('layouts.app')

@section('content')
<div class="jumbotron text-center">
<h1>{{$title}}</h1><hr><br><br>
<p><a class="btn btn-primary btn-primary" href="/login">Login</a> 
    <a class="btn btn-primary btn-success" href="/register">Register</a></p>
</div>
<div class="content">
<main role="main">
<!-- 
<div class="container">
  <!-- Example row of columns -->
  <!-- <div class="row">
    <div class="col-md-4">
      <h2>Heading</h2>
      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
      <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
    </div>
    <div class="col-md-4">
      <h2>Heading</h2>
      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
      <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
    </div>
    <div class="col-md-4">
      <h2>Heading</h2>
      <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
      <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
    </div>
  </div>

  <hr>

</div> /container -->

</main>
</div>
  <!-- <div class="row">
            <div class="col-md-12">
                <div id="news-slider9" class="owl-carousel">
                    <div class="post-slide9">
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo21/images/img-1.jpg" alt="">
                            <div class="over-layer">
                                <ul class="post-link">
                                    <li><a href="#" class="fa fa-search"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-review">
                            <h3 class="post-title"><a href="#">Latest News Post</a></h3>
                            <ul class="post-info">
                                <li>Comment: 2</li>
                                <li>Date: Feb 21, 2016</li>
                                <li>Author:Williamson</li>
                            </ul>
                            <ul class="tag-info">
                                <li>Tags:</li>
                                <li><a href="">Mockups,</a></li>
                                <li><a href="">Graphics,</a></li>
                                <li><a href="">Web Projects</a></li>
                            </ul>
                            <p class="post-description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eleifend a massa rhoncus gravida. Nullam in viverra sapien. Nunc bibendum nec lectus et vulputate. Nam.
                            </p>
                            <a href="#" class="read-more">read more</a>
                        </div>
                    </div>
     
                    <div class="post-slide9">
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo21/images/img-2.jpg" alt="">
                            <div class="over-layer">
                                <ul class="post-link">
                                    <li><a href="#" class="fa fa-search"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-review">
                            <h3 class="post-title"><a href="#">Latest News Post</a></h3>
                            <ul class="post-info">
                                <li>Comment: 4</li>
                                <li>Date: Feb 25, 2016</li>
                                <li>Author:Kristiana</li>
                            </ul>
                            <ul class="tag-info">
                                <li>Tags:</li>
                                <li><a href="">Mockups,</a></li>
                                <li><a href="">Graphics,</a></li>
                                <li><a href="">Web Projects</a></li>
                            </ul>
                            <p class="post-description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eleifend a massa rhoncus gravida. Nullam in viverra sapien. Nunc bibendum nec lectus et vulputate. Nam.
                            </p>
                            <a href="#" class="read-more">read more</a>
                        </div>
                    </div>
                    
                    <div class="post-slide9">
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo21/images/img-3.jpg" alt="">
                            <div class="over-layer">
                                <ul class="post-link">
                                    <li><a href="#" class="fa fa-search"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-review">
                            <h3 class="post-title"><a href="#">Latest News Post</a></h3>
                            <ul class="post-info">
                                <li>Comment: 4</li>
                                <li>Date: Feb 25, 2016</li>
                                <li>Author:Kristiana</li>
                            </ul>
                            <ul class="tag-info">
                                <li>Tags:</li>
                                <li><a href="">Mockups,</a></li>
                                <li><a href="">Graphics,</a></li>
                                <li><a href="">Web Projects</a></li>
                            </ul>
                            <p class="post-description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eleifend a massa rhoncus gravida. Nullam in viverra sapien. Nunc bibendum nec lectus et vulputate. Nam.
                            </p>
                            <a href="#" class="read-more">read more</a>
                        </div>
                    </div>
                    
                    <div class="post-slide9">
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo21/images/img-1.jpg" alt="">
                            <div class="over-layer">
                                <ul class="post-link">
                                    <li><a href="#" class="fa fa-search"></a></li>
                                    <li><a href="#" class="fa fa-link"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-review">
                            <h3 class="post-title"><a href="#">Latest News Post</a></h3>
                            <ul class="post-info">
                                <li>Comment: 4</li>
                                <li>Date: Feb 25, 2016</li>
                                <li>Author:Kristiana</li>
                            </ul>
                            <ul class="tag-info">
                                <li>Tags:</li>
                                <li><a href="">Mockups,</a></li>
                                <li><a href="">Graphics,</a></li>
                                <li><a href="">Web Projects</a></li>
                            </ul>
                            <p class="post-description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eleifend a massa rhoncus gravida. Nullam in viverra sapien. Nunc bibendum nec lectus et vulputate. Nam.
                            </p>
                            <a href="#" class="read-more">read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        

        <!-- Start Script-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
       
        <!-- Popper JS -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
@endsection

