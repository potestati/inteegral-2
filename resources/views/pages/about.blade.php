@extends('layouts.app')

@section('content')
<h1>{{$title}}</h1>

<p>ТЕСЛИАНУМ Д.О.О.</p> 
<p>Улица Николе Тесле, бр. 4 11080 Земун – Београд,</p>  
<p>Република Србија Матични број: 17151258  ПИБ број: 100427416 Основан: 1993. године</p>  
<p>Бонитетна 2018: AAA, </p> 
<p>Сертификат издат од стране CompanyWall, London, </p> 
<p>UK Оснивач, </p> 
<p>власник и директор - CEO: Мирјана Прљевић Регионални директор - CIO: Проф др Милош Димитријевић </p> 
 
<p>Фирма Теслианум д.о.о. је прва фирма у југоисточној Еврропи која је започела експертизу у домену пружања 
саветодавних услуга из области студија истраживања тржишта као основе дефинисања стратегије позиционирања 
више циљних група: компанија, малих и средњих предузећа, непрофитних организација и верских заједница, 
привредних комора и гранских асоцијација, градова и локалних самоуправа, магазина, и друго. </p>
 
<p>Сарадња фирме Теслианум д.о.о. је активна у односу на лобирање и обезбеђивање пројектних финансирања из 
ЕУ фондова као и других земаља и организација из региона Западног Балкана, као и региона црноморске и 
каспијске области. </p>
 
<p>Бројни пројекти и искуство тима ТЕСЛИАНУМ се огледа у прижању организационих услуга и подршке у оквиру 
интегралног приступа: различити стејкхолдери, од министарстава и комора, до локалних самоуправа и факултета, 
концептуализације пројектних задатака, као и реализације истих, 
првенствено кроз следеће пројекте: T²: TESLA SQUARE®, Академија малих генија TESLA GENIUS®, 
ТЕСЛИАНУМ® Алманах, INTEEGRAL: International Energy Efficiency Source of Knowledge,  
ГИМ СРБИЈЕ™: Георгафска Информативна Мрежа Србије у функцији привлачења ДСИ SCIENTIA: 
Small Cities Energy Efficient Innovatice Action, и други. 
Области у којима Теслианум тим поседује највише искуства стечено у 
последњих две деценије рада су: креирање стратешких оквира за реализацију различитих пројеката и дијалога, 
стратешко позиционирање, процесно стратешко планирање – контрола – мониторинг и примена 3К правила, 
енергетска ефикасност и енергетска безбедност, образовање младих – стартап обука. 
Мирјана Прљевић у Београду, 13, децембар 2018. година</p>

@endsection

