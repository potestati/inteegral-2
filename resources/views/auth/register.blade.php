@extends('layouts.app')

@section('content')

{{$errors}}

<div class="container-fluid">
    <div class="container">
        <div class="formBox">
            <form method="POST" action="{{ route('register') }}" novalidate>
                @csrf
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Registration / Registracija</h1>
                    </div>
                </div>
                <!--fullName-->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="inputBox ">
                            <div class="inputText">{{ __('Full Name / Ime i prezime') }}</div>
                            <input id="fullName" type="text" class="input{{ $errors->has('fullName') ? ' is-invalid' : '' }}" name="fullName" value="{{ old('fullName') }}" required autofocus>
                            @if ($errors->has('fullName'))
                            <span class="invalid-feedback" style="display:block">
                                <strong>{{ $errors->first('fullName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--companyName-->
                    <div class="col-sm-6">
                        <div class="inputBox ">
                            <div class="inputText">{{ __('
                                Company Name / Naziv kompanije') }}</div>
                            <input id="companyName" type="text" class="input{{ $errors->has('companyName') ? ' is-invalid' : '' }}" name="companyName" value="{{ old('companyName') }}" required autofocus>

                            @if ($errors->has('companyName'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('companyName') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <!--mb-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Identification Number / Maticni broj') }}</div>
                            <input id="mb" type="text" class="input{{ $errors->has('mb') ? ' is-invalid' : '' }}" name="mb" value="{{ old('mb') }}" required autofocus>
                            @if ($errors->has('mb'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('mb') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--pib-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Task Number / PIB') }}</div>
                            <input id="pib" type="text" class="input{{ $errors->has('pib') ? ' is-invalid' : '' }}" name="pib" value="{{ old('pib') }}" required autofocus>
                            @if ($errors->has('pib'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('pib') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <!--contactPerson-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Contact Person / Kontakt Osoba') }}</div>
                            <input id="contactPerson" type="text" class="input{{ $errors->has('contactPerson') ? ' is-invalid' : '' }}" name="contactPerson" value="{{ old('contactPerson') }}" required autofocus>
                            @if ($errors->has('contactPerson'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('contactPerson') }}</strong>
                            </span>
                            @endif
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--website-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('website') }}</div>
                            <input id="website" type="text" class="input{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" value="{{ old('website') }}" required autofocus>
                            @if ($errors->has('website'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('website') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <!--address-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('address') }}</div>
                            <input id="address" type="text" class="input{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required autofocus>
                            @if ($errors->has('address'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                {{-- dodati u bazu polja za kontakt telefon i za dodatan komentar--}}
                <div class="row">
                    <!--contact phone no-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Contact phone/Kontakt No. Tel: +381') }}</div>
                            <input id="phoneNo" type="text" class="input{{ $errors->has('phoneNo') ? ' is-invalid' : '' }}" name="phoneNo" value="{{ old('phoneNo') }}" required>

                            @if ($errors->has('phoneNo'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('phoneNo') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    {{-- additional data --}}
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Additional data / Dodatni podaci') }}</div>
                            <input id="additionalData" type="text" class="input{{ $errors->has('additionalData') ? ' is-invalid' : '' }}" name="additionalData" value="{{ old('additionalData') }}" required autofocus>

                            @if ($errors->has('additionalData'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('additionalData') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--email-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('E-Mail Address') }}</div>
                            <input id="email" type="email" class="input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                {{-- username--}}
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('username') }}</div>
                            <input id="username" type="text" class="input{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                            @if ($errors->has('username'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--password-->
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Password') }}</div>
                            <input id="password" type="password" class="input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required>

                            @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                     {{-- confirm password--}}
                    <div class="col-sm-6">
                        <div class="inputBox">
                            <div class="inputText">{{ __('Confirm Password') }}</div>
                            <input id="password-confirm" type="password" class="input{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" value="{{ old('password_confirmation') }}" required autofocus>

                            @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" class="button" value="Register">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Start Script-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<!-- Latest compiled JavaScript -->
{{--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>  --}}
<!-- Main Script -->
<script src="js/main.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript">
    $(".input").focus(function () {
        $(this).parent().addClass("focus");
    })
</script>
<!-- / Script-->

@endsection
