@extends('layouts.app')
@section('content')
<h1>Edit Category</h1>
{!! Form::open(['action' => ['CategoriesController@update', $category->id], 'method' => 'POST']) !!}
<div class="form-group">
    {{Form::label('categoryName', 'Category Name')}}
    {{Form::text('categoryName', $category->categoryName, ['class' => 'form-control', 'placeholder' => 'Category Name'])}}
</div>
<div class="form-group">
    {{Form::label('categoryValue', 'Category Value')}}
    {{Form::text('categoryValue', $category->categoryValue, ['class' => 'form-control', 'placeholder' => 'Category Value'])}}
</div>
<div class="form-group">
    {{Form::label('categoryDescription', 'Category Description')}}
    {{Form::text('categoryDescription', $category->categoryDescription, ['class' => 'form-control', 'placeholder' => 'Category Description'])}}
</div>
<div class="form-group">
    {{Form::label('categoryManager', 'Category Manager')}}
    {{Form::text('categoryManager', $category->categoryManager, ['class' => 'form-control', 'placeholder' => 'Category Manager'])}}
</div>
<div class="form-group">
    {{Form::label('legalRegulations', 'Legal Regulations')}}
    {{Form::text('legalRegulations', $category->legalRegulations, ['class' => 'form-control', 'placeholder' => 'Legal Regulations'])}}
</div>
<div class="form-group">
    {{Form::label('categoryPotencial', 'Category Potencial')}}
    {{Form::text('categoryPotencial', $category->categoryPotencial, ['class' => 'form-control', 'placeholder' => 'Category Potencial'])}}
</div>
<div class="form-group">
    {{Form::label('contactData', 'Contact Data')}}
    {{Form::text('contactData', $category->contactData, ['class' => 'form-control', 'placeholder' => 'Contact Data'])}}
</div>
<div class="form-group">
    {{Form::label('businessArea', 'Business Area')}}
    {{Form::text('businessArea', $category->businessArea, ['class' => 'form-control', 'placeholder' => 'Business Area'])}}
</div>
<div class="form-group">
    {{Form::label('scaleValue', 'Scale Value')}}
    {{Form::text('scaleValue', $category->scaleValue, ['class' => 'form-control', 'placeholder' => 'Scale Value'])}}
</div>
    {{-- 
    <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $category->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body'])}}
    </div>
    --}}
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}

    {!! Form::close() !!}

    @endsection
