<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/hello', function () {
    /*    return view('welcome');*/
    return ' Hello ';
});
Route::get('/about', function () {
    return view('pages.about');
    /*    return ' Hello ';*/
});
/*  http://laravelapp.local/users/Brad u url
output je This is user Brad*/
/*Route::get('/users/{id}', function ($id) {
    return 'This is user ' . $id;
});*/
/* http://laravelapp.local/users/2/Brad ovo je u url adresi za pozivanje stranice za ovu dole rutu*/
Route::get('/users/{id}/{name}', function ($id, $name) {
    return 'This is user ' . $name . 'with an id of '. $id;
});
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');
Route::get('/home', 'PagesController@home')->name('homeStart');
Route::get('/category/{catName}', 'OblastController@index');
//Route::get('/category/{catName}', 'PagesController@availableProjects');
Route::get('/availableProjects/{projectSlug}', 'PagesController@project');

Route::post('/project/add', 'ProjectController@addProject')->name('addProject');
Route::post('/project/{id}', 'ProjectController@updateProject')->name('updateProject');

Route::get('/availableProjects/{projectSlug}/upit/{projectId}', 'PagesController@showUpit')->name('upit'); 
Route::post('/upit/add', 'UpitController@addUpit')->name('addUpit');
// Dashboard
Route::get('/dashboard/availableProjects', 'AvailableProjectsController@showAvailableProjectsDashboard')->name('showAvailableProjectsDashboard');
Route::post('/dashboard/availableProjects', 'AvailableProjectsController@create')->name('createAvailableProjects');
//kreiran sa naredbom $ php artisan make:controller PostController --resource
Route::resource('posts', 'PostsController');
Route::resource('projects', 'ProjectsController');
// Admin / Dashboard panel
Auth::routes();
Route::get('/admin/user/roles',['middleware'=>['role', 'auth', 'web'], function(){
    return "Middleware roles";
}]);
Route::get('/dashboard', 'DashboardController@index')->name('dashboardHome');
Route::get('/dashboard/projectReview/{id}', 'DashboardController@showprojectReview')->name('projectReview');
Route::post('/dashboard', 'DashboardController@addStatus')->name('addStatus');
