<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('projectName');
            $table->string('projectSector');
            $table->string('projectValue');
            $table->string('selectedMap');
            $table->string('projectManager');
            $table->string('contactData');
            $table->string('website');
            $table->string('email');
            $table->string('address');
            $table->string('projectDescription');
            $table->string('projectCharacter');
            $table->string('regionCharacter');
            $table->string('offeredCooperation');
            $table->string('certificates');
            $table->string('contactPerson');
            $table->string('status');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
