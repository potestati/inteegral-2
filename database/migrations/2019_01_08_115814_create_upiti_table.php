<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpitiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upiti', function (Blueprint $table) {
            $table->increments('id');
            $table->string('projectName');
            $table->string('projectValue');
            $table->string('projectDescription');
            $table->string('companyDescr');
            $table->string('regionDescr');
            $table->string('projectManager');
            $table->string('legalRegulations');
            $table->string('projectPotencial');
            $table->string('contactData');
            $table->string('businessArea');
            $table->string('scaleValue');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')
                ->references('id')->on('project')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upiti');
    }
}
