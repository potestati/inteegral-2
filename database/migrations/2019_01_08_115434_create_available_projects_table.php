<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('projectName');
            $table->string('slug');
            $table->boolean('status');
            $table->string('amount');
            $table->integer('oblasti_id')->unsigned();
            $table->foreign('oblasti_id')
                ->references('id')->on('oblasti')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_projects');
    }
}
