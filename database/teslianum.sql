-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2019 at 04:13 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teslianum`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `job_title`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', 'admin', 'adminadmin', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `available_projects`
--

CREATE TABLE `available_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `projectName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `available_projects`
--

INSERT INTO `available_projects` (`id`, `projectName`, `slug`, `status`, `category_id`, `created_at`, `updated_at`) VALUES
(18, 'project 1', 'project-1', 1, 1, NULL, NULL),
(19, 'project 1', 'project-1', 1, 1, NULL, NULL),
(20, 'project 5', 'project-5', 1, 1, NULL, NULL),
(21, 'project 6', 'project-6', 0, 2, NULL, NULL),
(22, 'project 7', 'project-7', 0, 3, NULL, NULL),
(23, 'project 8', 'project-8', 0, 4, NULL, NULL),
(24, 'project 9', 'project-9', 1, 1, NULL, NULL),
(25, 'project 10', 'project-10', 0, 2, NULL, NULL),
(26, 'project 11', 'project-11', 1, 3, NULL, NULL),
(27, 'project 12', 'project-12', 0, 4, NULL, NULL),
(28, 'project 13', 'project-13', 1, 1, NULL, NULL),
(29, 'project 14', 'project-14', 0, 2, NULL, NULL),
(30, 'project 1', 'project-1', 1, 1, NULL, NULL),
(31, 'project 2', 'project-2', 0, 2, NULL, NULL),
(32, 'project 3', 'project-3', 1, 3, NULL, NULL),
(33, 'project 4', 'project-4', 0, 4, NULL, NULL),
(34, 'project 5', 'project-5', 1, 1, NULL, NULL),
(35, 'project 6', 'project-6', 0, 2, NULL, NULL),
(36, 'asdasdsa', 'asdasd', 1, 1, '2018-07-19 15:21:30', '2018-07-19 15:21:30'),
(37, 'Neki projekat', 'neki-projekat', 1, 2, '2018-09-06 13:18:43', '2018-09-06 13:18:43'),
(38, 'Neki projekat', 'neki-projekat', 1, 2, '2018-09-06 13:20:15', '2018-09-06 13:20:15'),
(39, 'Neki projekat', 'neki-projekat', 1, 2, '2018-09-06 13:20:44', '2018-09-06 13:20:44'),
(40, 'Neki projekat', 'neki-projekat', 1, 2, '2018-09-06 13:21:14', '2018-09-06 13:21:14'),
(41, 'aaaaa', 'aaaaa', 1, 2, '2018-09-06 13:21:23', '2018-09-06 13:21:23'),
(42, 'asdasd', 'asdasd', 1, 3, '2018-09-06 13:22:07', '2018-09-06 13:22:07'),
(43, 'Project NOvi', 'project-novi', 1, 3, '2018-09-12 06:40:56', '2018-09-12 06:40:56'),
(44, 'NOvi', 'novi', 1, 2, '2018-09-12 06:42:38', '2018-09-12 06:42:38'),
(45, 'NOvi Novi', 'novi-novi', 1, 2, '2018-09-12 12:13:12', '2018-09-12 12:13:12'),
(46, 'test projekat', 'test-projekat', 1, 3, '2018-12-24 18:06:21', '2018-12-24 18:06:21'),
(47, 'Aktivni projekat', 'aktivni-projekat', 1, 1, '2018-12-25 09:59:54', '2018-12-25 09:59:54'),
(48, 'asdfasdf', 'asdfasdf', 1, 4, '2018-12-26 08:05:59', '2018-12-26 08:05:59'),
(49, 'test test test test', 'test-test-test-test', 1, 4, '2018-12-26 08:06:43', '2018-12-26 08:06:43'),
(50, 'NOVI TEST PROJEKAT', 'novi-test-projekat', 1, 4, '2018-12-27 17:38:32', '2018-12-27 17:38:32'),
(51, 'raspoloziv projekat test', 'raspoloziv-projekat-test', 1, 1, '2018-12-27 18:13:22', '2018-12-27 18:13:22'),
(52, 'novi projekat', 'novi-projekat', 1, 3, '2018-12-28 10:55:34', '2018-12-28 10:55:34'),
(53, 'investicioni projekat', 'investicioni-projekat', 1, 1, '2018-12-30 10:21:29', '2018-12-30 10:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryDescription` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryImage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categoryName`, `slug`, `categoryDescription`, `categoryImage`, `created_at`, `updated_at`) VALUES
(1, 'MAPA STRATEŠKIH PARTNERSTAVA', 'mapa-strateskih-partnerstava', 'MAPA STRATEŠKIH PARTNERSTAVA', 'red-serbia.png', NULL, NULL),
(2, 'MAPA INVESTICIONIH PROJEKATA', 'mapa-investicionih-projekata', 'MAPA INVESTICIONIH PROJEKATA', 'yellow-serbia.png', NULL, NULL),
(3, 'MAPA PROIZVODA I USLUGA', 'mapa-proizvoda-i-usluga', 'MAPA PROIZVODA I USLUGA', 'green-serbia.png', NULL, NULL),
(4, 'MAPA PATENATA I INOVACIJA', 'mapa-patenata-i-inovacija', 'MAPA PATENATA I INOVACIJA', 'blue-serbia.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2018_06_12_112926_create_users_table', 1),
(12, '2018_06_12_113013_project', 1),
(13, '2018_06_12_113821_categories', 1),
(14, '2018_06_12_113837_upiti', 1),
(15, '2018_06_12_113900_available_projects', 1),
(16, '2018_12_09_114113_create_admins_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(2, 'Post two', 'This is post two', '2018-05-25 07:56:54', '2018-05-25 07:56:54'),
(3, 'Post Three', 'Body Text for post three', '2018-05-26 08:08:16', '2018-05-26 08:08:16'),
(4, 'Informacija', '<blockquote>\r\n<p><strong>Hello</strong> from four<em>th<strong> post</strong></em></p>\r\n</blockquote>', '2018-05-26 08:43:55', '2018-12-26 07:45:27'),
(5, 'Post nova', 'asdfasdfasdf', '2018-12-17 19:24:21', '2018-12-17 19:24:21'),
(6, 'Informacija', 'sdfasdf', '2018-12-26 07:45:41', '2018-12-26 07:45:41'),
(7, 'Naziv otvorenog projekta : MUZEJ PIVA - zaštita  i obnova  industrijskog nasleđa', '<h4>Map of Investments – Mapa Investicija</h4>\r\n<br>\r\n<h4>Sector of Economy/Oblast</h4>\r\n<br>	\r\n<h4>Turizam</h4>\r\n<hr>\r\n<br>\r\nProject value (EUR)/Vrednost<br>	\r\nEUR  108.280 €, <br>\r\npo skali - od 100.001 – 500.000 €<br>\r\n<br><br><br>\r\nYou apply with project as/ Odabrana Mapa\r\n<hr>	 \r\nInvesticiono ulaganje\r\n<br>\r\n<br>\r\nProject Manager/Funkcija<br>\r\nVojislav Cvejić, ecc., saradnik za turizam, inicijator projekta, realizator prve faze projekta koja je realizovana preko udruženja građana „Urbani forum“ konkurisanjem za sredstva iz budžeta grada Zrenjanina, rešenjem gradonačelnika od 24. marta 2016. godine, broj rešenja: 451-2/2016-98-II.\r\n<br><br>\r\nContact phone/Kontakt	Tel:  +381 (023) 523-160 , mobilni tel. 060-506-11-20<br>\r\nWeb site <br> 	 visitzrenjanin.com\r\nE-mail<br>	vojislav.cvejic@visitzrenjanin.com\r\n<br><br><br><br>\r\n\r\n\r\nDescription / Opis projekta	  <br>\r\n<br>\r\nDavanje društveno korisne namene najvažnijem sačuvanom pivskom pogonu tzv. varioni i prenamena u MUZEJ PIVA, revitalizacija kulturno-istorijskog nasleđa, razvoj pivskog turizma preko cele godine u gradu Zrenjaninu, zaposlenje velikog broja građana u više oblasti, povezivanje sa nautičkim turizmom jer je lokacija objekta koji je obuhvaćen projektom na reci Begej, nadovezivanje ovog projekta na manifestaciju „DANI PIVA“ - (zaokruživanje celog turističkog proizvoda), uz dolazak velikog broja turista očekivani veliki finansijski priliv budžetu grada Zrenjanina.\r\nKonkretno  traži  se mogućnost završetka revitalizacije i adaptacije dela pivare koji je predviđen za Muzej piva. Pošto je prva faza revitalizacije obavljena sredstvima grada Zrenjanina koji nije u mogućnosti da u potpunosti finansira projekat. Potrebno je postojeće mašine konzervirati, renovirati unutrašnjost 3 etaže i delimično 1 etažu, na 1 etaži urediti kongresnu salu sa pratećom tehničkom opremom (video projektor sa platnom, računar, ozvučenje, stolice  itd.).  Sala je namenjena za stručne skupove u vezi pivarstva. Renovirati 1 preostalu fasadu u dvorištu sa iskorišćenjem postojeće terase, obnoviti instalacije, postaviti sanitarne čvorove  itd. (sve nabrojano se nalazi u DETALJNOM ELABORATU koji je izrađen).\r\n<br><br><br><br>\r\n 	\r\n 	\r\n 	\r\n 	\r\n 	\r\n 	\r\n 	\r\n 	\r\n 	\r\n 	\r\n\r\n\r\n\r\nSpecify characteristics of the product  \r\n<hr>\r\n<br><br>\r\nSpecifične karakteristike proizvoda ili projekta ili patenta<br>\r\n<br>	\r\nU široj okolini ne  postoji ovakav muzej o pivarstvu koji poseduje osim sitnih eksponata, arhivske građe, alata, sirovina, dokumenata i  sačuvan proces proizvodnje sa  kompletnom opremom i mašinama na 4 sprata zgrade. Većinu opreme ugradila je jedna od najpoznatijih firmi u svetu za proizvodnju pivarske opreme a to je: nemačka firma ZIEMANN SUDWERK iz grada Ludwigsburga. Ova firma je 2009. godine ugradila svoju opremu u najvećoj pivari na svetu u Meksiku. Enterijer celog prostora gde se kuvalo pivo (podovi i zidovi) urađen je u bračkom kamenu u 9 različitih boja. Sa ovim kamenom uređeno je predvorje UN u Nju Jorku, palata Srbija u Novom Beogradu, palata Izvršnog veća Ap Vojvodine, palata parlamenta u Budimpešti itd.   Takođe ovaj muzej bazira se na poštovanju tradicije u proizvodnji piva sa tradicionalnim recepturama. Korišćena je PLZANSKA TEHNOLOGIJA sa potpuno prirodnim sirovinama. Ceo pivskih industrijski kompleks zajedno sa očuvanom starom palatom Lazara Dunđerskog je vredno kulturno-istorijsko i industrijsko nasleđe Republike Srbije i predstavlja jedan od najvećih arhitektonski očuvanih pivskih građevina u celoj državi. Projektanti ovog kompleksa bili su iz Češke, firma JAN & NOVAK - PRAG. Nalazi se na samoj obali reke Begej što daje posebnost. Uz manifestaciju „DANI PIVA“ čini jedan poseban turistički proizvod kojeg koriste koisnici svih uzrasta. \r\n<br><br><br>\r\n\r\n 	\r\n	\r\n\r\nSpecify characteristics of the region and the benefits that the region may have from a realization of the project /\r\n<br>\r\n<hr>\r\n<br>\r\nSpecifične karakteristike regiona i koristi koje bi Grad – Opština dobila realizacijom ovog projekta\r\nSpecifične karakteristike regiona: region Srednjeg Banata kao i šira oblast banatske regije poznata je od davnina po proizvodnji piva i ta tradicija datira još sa početka 18. veka. Najstarije pivare u ovom delu Evrope su upravo u ovom regionu  i to: 1. Temišvarska pivara osnovana 1718. godine, 2. Pančevačka pivara osnovana 1722. godine, Vršačka pivara osnovana 1742. godine, Zrenjaninska pivara osnovana 1745. godine. Iako nije zrenjaninska pivara najstarija u ovoj grupaciji njen prvi vlasnik Sebastijan Kracajzen, Nemac, kupio je i najstariju pivaru u našoj zemlji – pančevačku pivaru, kao i zemunsku pivaru, tako da je imao svoje 3 pivare. Primera radi zrenjaninska pivara je starija 102 godine od čuvene Carlsberg pivare iz Kopenhagena i 124 godine od čuvene Staropramen pivare iz Praga. Osim toga region Srednjeg Banata karakteriše i najgušća vodena mreža u Evropi  i u krugu od 30 kilometara nalazi se preko 6 vodenih tokova – reka i kanala: Begej, Tisa, Tamiš, Dunav, Dunavac, Brzava  i kanal DTD  sa više veoma velikih jezerskih površina, ribnjaka (ribnjak Ečka je 2. u Evropi) i rezervata prirode. Voda je osnovna sirovina u proizvodnji piva a prema rečima stručnjaka iz Nemačke iz grada Esena (Privatbrauerei Jacob Stauder) grad Zrenjanin ima veliku sreću da kroz njega protiče reka sa najpogodnijom vodom tzv. „mekom vodom“ za proizvodnju najkvalitetnijeg piva. U ovako gustoj vodnoj mreži ovaj region karakterišu  i  5 zaštićenih prirodnih područja (rezervata prirode, parkova prirode) i jednim u osnovanju sa bogatom florom i faunom. Ove 2 karakteristike su međusobno povezane jer osim osnovne sirovine za proizvodnju piva – najbolje vode, stara pivara i objekat obuhvaćen projektom se nalaze uz samu reku Begej. Zato radi održivosti projekta predviđeno je povezivanje pivskog i nautičkog turizma ali i umrežavanje sa ostalim vidovima turizma: eko turizmom (rezervati prirode), kulturnim turizmom, biciklističkim, banjskim, manifestacionim (festival piva „DANI PIVA“) itd.\r\n<br><br><br>\r\n\r\nKoristi koje bi Grad – Opština dobila realizacijom ovog projekta:<br>\r\n<br>\r\nDavanje društveno korisne namene najvažnijem sačuvanom pivskom pogonu tzv. varioni i prenamena u MUZEJ PIVA (projekat je vec zapocet sredstvima grada Zrenjanina kao i donacijama), revitalizacija kulturno-istorijskog nasleđa, razvoj pivskog turizma preko cele godine u gradu Zrenjaninu, zaposlenje velikog broja građana u više oblasti, povezivanje sa nautičkim turizmom jer je lokacija objekta obuhvaćenim projektom na reci Begej, nadovezivanje ovog projekta na manifestaciju „DANI PIVA“ - jednom od najvećih u Republici Srbiji sa oko 500.000 posetilaca. (zaokruživanje celog turističkog proizvoda), uz dolazak velikog broja turista očekivani veliki finansijski priliv budžetu grada Zrenjanina. Poslednjih godina od kada zrenjaninska pivara ne radi postoji veliko nezadovoljstvo i ogorčenje među građanima Zrenjanina i okolnih mesta koji u nekoliko grupa deluju i udružuju se da sačuvaju tradiciju staru gotovo 300 godina. Postojala je realna šansa da najvrednija manifestacija grada Zrenjanina “DANI PIVA” izgubi smisao jer se organizuje festival piva bez sopstvenog proizvoda - piva ?! Već su drugi gradovi u okolini preuzeli ideju festivala piva. Iz svih tih ozbiljnih činjenica je i pokrenuta prva faza spašavanja vrednog industrijskog nasleđa deo stare pivare i njeno pretvaranje u MUZEJ PIVA sa PLANOM INSTALIRANJA MINI – CRAFT PIVARE. Ovim projektom grad Zrenjanin bi očuvao tradiciju staru skoro 3 veka, obnovio najjači BREND GRADA  i   kroz razvoj pivskog turizma značajno dobio finansijski priliv u budžet grada. Tu su razni finansijski prilivi gradu od poreza, zakupa poslovnog prostora, parkinga itd. Temelj - okosnica - stub  i „magnet“ koji će celu turističku ponudu grada Zrenjanina držati „na okupu“ je PIVSKI TURIZAM. On je taj koji grad Zrenjanin razlikuje od drugih gradova sa njihovim turističkim proizvodima i kao UNIKATAN I ORIGINALAN može da bude privlačan velikom broju posetilaca.<br><br>\r\nKao što je istaknuto pivski turizam u gradu Zrenjaninu koji se već povezuje sa ostalim vidovima turizma sigurno može da uposli veliki broj građana u raznim oblastima i to u: proizvodnji domaćeg kraft piva, u proizvodnji ambalaže, sirovina, poljoprivrednih sirovina, u hotelijerstvu, ugostiteljstvu, trgovini, prevozu, izradi suvenira, proizvodnji hrane i raznim drugim uslugama.\r\n\r\n<br><br><br>\r\n\r\n\r\nCooperation offered to a foreign partner or investor<br>\r\n\r\nSaradnja koju nudite stranom partneru, odnosno investitoru<br><br>\r\nPovezivanje sa stranim pivskim gradovima i regijama, poseta - razmena turista na njihovom festivalu piva, plasman stranih proizvoda na našem festivalu piva, predstavljanje turističke ponude zemlje – mesta, grada stranog partnera kod nas, predstavljanje ostalih resursa stranog partnera kod nas (privrednih, kulturnih, sportskih itd.), mogućnost investiranja stranog partnera u našem gradu i okolini pod izuzetno povoljnim uslovima npr. u industrijskim zonama itd.\r\n\r\n\r\n\r\n	\r\n\r\n\r\n\r\n<br><br><br>\r\nLegal regulations (certificates, patent rights, land ownership, etc.)\r\n<br>\r\nPravni okvir i zakonodavna osnova predloga (sertifikati, patentna prava, vlasništvo nad zemljom, i drugo) 	\r\n<br>\r\nObjekat i zemljište su vlasništvo grada Zrenjanina – JAVNA SVOJINA. Katastarska opština: K.O. Zrenjanin I, broj parcele/objekta: 6638/1 objekat broj 3, površina parcele/objekta: 5 a 36 kvm. Podaci se nalaze u posedovnom listu IZVODA iz lista nepokretnosti broj: 22540\r\n <br><br><br>	\r\n\r\nContact person/ Kontakt osoba\r\n<br>\r\nVojislav Cvejić, Turistička organizacija grada Zrenjanina,<br>\r\nTel. 023-523-160<br>\r\nMob. tel. 060-506-11-20<br>', '2018-12-31 10:17:21', '2018-12-31 10:26:41');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(10) UNSIGNED NOT NULL,
  `projectName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectSector` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectValue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `selectedMap` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectManager` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactData` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectDescription` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectCharacter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regionCharacter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offeredCooperation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificates` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactPerson` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `projectName`, `projectSector`, `projectValue`, `selectedMap`, `projectManager`, `contactData`, `website`, `email`, `address`, `projectDescription`, `projectCharacter`, `regionCharacter`, `offeredCooperation`, `certificates`, `contactPerson`, `user_id`, `created_at`, `updated_at`, `category_id`, `status`) VALUES
(1, 'sadasd', 'asdasdasd', 'sdasd', 'sadasdad', 'sadsad', 'asdasds', 'sadasdasd', 'sadasdas', 'wsdsadsa', 'sadasdasd', 'sadasdds', 'sadasdaas', 'sadada', 'sdadada', 'asdadasdas', 1, '2018-07-08 15:52:36', '2018-07-08 15:52:36', 1, 1),
(2, 'asdasd', 'asdasda', 'asdasd', 'asdasd', 'dadasd', 'sdasd', 'sdadasd', 'dsdasd', 'dsadasd', 'dsadasd', 'dadasd', 'asdada', 'asdasd', 'asdasd', 'sadasdasd', 1, '2018-07-08 15:55:21', '2018-07-08 15:55:21', 1, 1),
(3, 'asdsad', 'sadsad', 'sadasd', 'asdsa', 'sadas', 'asda', 'sdasd', 'asda', 'sdsad', 'sadsad', 'sadsadas', 'asd', 'sadasd', 'asdasd', 'asdasd', 1, '2018-07-19 15:16:25', '2018-07-19 15:16:25', 1, 2),
(5, 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 'STATUS', 1, '2018-09-05 09:13:25', '2018-12-27 19:34:38', 1, 1),
(6, 'asdfa', 'asdfasdf', 'asdfasdf', 'asdfasd', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasd', 'asdf', 'asdf', 'asdf', 'asdfasdf', 2, '2018-12-19 15:08:42', '2018-12-19 15:08:42', 3, 1),
(7, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'ana.komazec@gmail.com', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 2, '2018-12-23 09:19:15', '2018-12-23 09:19:15', 2, 0),
(8, 'test', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'aniko@gmail.com', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 1, '2018-12-24 16:08:16', '2018-12-24 16:08:16', 2, 0),
(9, 'adsf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'ana.komazec@gmail.com', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 1, '2018-12-24 17:18:38', '2018-12-24 17:18:38', 2, 1),
(10, 'asdfasdf', 'asdfasdff', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfsadf', 'potestati@gmail.com', 'asdfasdf', 'asdfasdf', 'asdasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-25 09:52:48', '2018-12-25 09:52:48', 3, 2),
(11, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'potestati@gmail.com', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 1, '2018-12-25 09:59:14', '2018-12-25 09:59:14', 3, 0),
(12, 'asdf', 'asdf', 'asd', 'asdf', 'asdf', 'asdf', 'asdf', 'marko.radonic@outlook.com', 'asdf', 'asdf', 'adf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-25 10:33:02', '2018-12-25 10:33:02', 1, 0),
(13, 'asdsad', 'sadsad', 'sadasd', 'asdsa', 'sadas', 'asda', 'sdasd', 'asda', 'sdsad', 'sadsad', 'sadsadas', 'asd', 'sadasd', 'asdasd', 'asdasd', 1, '2018-12-25 10:37:58', '2018-12-25 10:37:58', 1, 0),
(14, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'ana.komazec@gmail.com', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-25 10:41:27', '2018-12-25 10:41:27', 1, 0),
(15, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'ana.komazec@gmail.com', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 1, '2018-12-25 10:42:38', '2018-12-25 10:42:38', 2, 2),
(16, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'ana.komazec@gmail.com', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 1, '2018-12-25 10:44:43', '2018-12-25 10:44:43', 2, 0),
(17, 'adfadf', 'asdfasdf', 'adsfa', 'asdfasdf', 'asdfasdf', 'asdfa', 'asdfadf', 'potestati@gmail.com', 'asdfasf', 'asdfasdf', 'asdfadsf', 'asdfadfs', 'asdfadf', 'asdfassdf', 'asdfadf', 1, '2018-12-26 07:46:48', '2018-12-26 07:46:48', 1, 0),
(18, 'adfadf', 'asdfasdf', 'adsfa', 'asdfasdf', 'asdfasdf', 'asdfa', 'asdfadf', 'potestati@gmail.com', 'asdfasf', 'asdfasdf', 'asdfadsf', 'asdfadfs', 'asdfadf', 'asdfassdf', 'asdfadf', 1, '2018-12-26 07:47:01', '2018-12-26 07:47:01', 1, 0),
(19, 'asdfadsf', 'asdfasdf', 'asdfadf', 'asdfadf', 'asdfadsf', 'asdfadsf', 'asdfadsf', 'marko.radonic@outlook.com', 'asdfadsf', 'asdfasdf', 'adsfadf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfadf', 1, '2018-12-26 07:47:50', '2018-12-26 07:47:50', 3, 0),
(20, 'ADSFA', 'asdfa', 'adfa', 'asdfa', 'adsf', 'asdfasdf', 'asdfasdf', 'potestati@gmail.com', 'asdfadsf', 'asdfasdf', 'asdfadsf', 'asdfasdf', 'asdfadf', 'asdfadf', 'asdfasdf', 1, '2018-12-26 08:00:07', '2018-12-26 08:00:07', 1, 0),
(21, 'asdfads', 'asdfasdf', 'adfasd', 'asdfadf', 'adfasd', 'asdfasdf', 'asdfadf', 'marko.radonic@outlook.com', 'asdfadf', 'asdfadsf', 'asdfasdf', 'asdfadf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-26 08:04:15', '2018-12-26 08:04:15', 3, 0),
(22, 'asdsad', 'sadsad', 'sadasd', 'asdsa', 'sadas', 'asda', 'sdasd', 'asda', 'sdsad', 'sadsad', 'asdfasdfasdf', 'asd', 'sadasd', 'asdasd', 'asdasd', 1, '2018-12-26 08:05:19', '2018-12-26 08:05:19', 1, 0),
(23, 'asdfadf', 'sadfasdf', 'dfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'potestati@yahoo.com', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'sadfasdf', 1, '2018-12-27 16:28:53', '2018-12-27 16:28:53', 3, 0),
(24, 'sadasd', 'asdasdasd', 'sdasd', 'sadasdad', 'sadsad', 'asdasds', 'sadasdasd', 'sadasdas', 'wsdsadsa', 'sadasdasd', 'sadasdds', 'sadasdaas', 'sadada', 'sdadada', 'asdadasdas', 1, '2018-12-27 17:38:07', '2018-12-27 17:38:07', 1, 0),
(25, 'sadasd', 'asdasdasd', 'sdasd', 'sadasdad', 'sadsad', 'asdasds', 'sadasdasd', 'sadasdas', 'wsdsadsa', 'sadasdasd', 'sadasdds', 'sadasdaas', 'sadada', 'sdadada', 'asdadasdas', 1, '2018-12-27 17:59:10', '2018-12-27 17:59:10', 1, 0),
(26, 'asdfadsf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-27 18:08:26', '2018-12-27 18:08:26', 3, 0),
(27, 'adfadsf', 'asdfadf', 'asdfadsf', 'asdfadf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'test@gmail.com', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-27 18:29:10', '2018-12-27 18:29:10', 3, 0),
(28, 'asdfasdf', 'asdfadsf', 'asdfasdf', 'asdfasd', 'asdfasdf', 'asdfasdf', 'asdfasd', 'marko.radonic@outlook.com', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'adf', 'asdf', 'asd', 1, '2018-12-27 18:50:27', '2018-12-27 18:50:27', 2, 0),
(29, 'sadasd', 'asdasdasd', 'sdasd', 'sadasdad', 'sadsad', 'asdasds', 'sadasdasd', 'sadasdas', 'wsdsadsa', 'sadasdasd', 'sadasdds', 'sadasdaas', 'sadada', 'sdadada', 'asdadasdas', 1, '2018-12-27 19:00:35', '2018-12-27 19:00:35', 1, 0),
(30, 'asdffa', 'asdfadfsd', 'adfadf', 'asdfadff', 'asdfafd', 'asdfadf', 'asdfaf', 'testtest@gmail.com', 'asdfasdff', 'asdfasdff', 'asdfadsf', 'adfadf', 'adfadf', 'asdfasddf', 'asdfasdf', 4, '2018-12-28 10:52:43', '2018-12-28 10:52:43', 2, 0),
(31, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'marko.radonic@outlook.com', 'asdfasdf', 'asdfasdfasdf', 'asdfadsf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 1, '2018-12-30 15:16:44', '2018-12-30 15:16:44', 3, 0),
(32, 'adsfasdff', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdff', 'asdfasdf', 'asdfasdff', 'asdfasdf', 'asdfasdf', 'asdffasdf', 'asdfasdf', 'asdfasdff', 'sadfasdf', 'asdfasdf', 1, '2018-12-30 16:12:40', '2018-12-30 16:12:40', 3, 0),
(33, 'TESTTESTTEST', 'sdfgsfdg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'potestati@gmail.com', 'sdfgsdfg', 'sdfgsdfgsdf', 'sdfgsdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 5, '2019-01-02 16:01:38', '2019-01-02 16:04:57', 2, 0),
(34, 'asdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'potestati@gmail.com', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdfasdf', 'asdfasdf', 'asdfasdf', 1, '2019-01-02 16:45:17', '2019-01-02 16:45:17', 3, 0),
(35, 'sdfgsdf', 'sdfgsdfg', 'sdfgsdf', 'gsdfgsdfg', 'sdfgsdf', 'gsdfgsdfg', 'sdfgsdfg', 'ana.komazec@gmail.com', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 1, '2019-01-04 14:11:46', '2019-01-04 14:11:46', 2, 0),
(36, 'adfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'test@gmail.com', 'sdfgsfdg', 'sdfgsfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 1, '2019-01-04 14:12:39', '2019-01-04 14:12:39', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `roleName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'user', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `upiti`
--

CREATE TABLE `upiti` (
  `id` int(10) UNSIGNED NOT NULL,
  `projectName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectValue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectDescription` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyDescr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regionDescr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectManager` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legalRegulations` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `projectPotencial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactData` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `businessArea` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scaleValue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upiti`
--

INSERT INTO `upiti` (`id`, `projectName`, `projectValue`, `projectDescription`, `companyDescr`, `regionDescr`, `projectManager`, `legalRegulations`, `projectPotencial`, `contactData`, `businessArea`, `scaleValue`, `project_id`, `created_at`, `updated_at`) VALUES
(1, 'aaadsasd', 'adsaasda', 'sdasdasd', 'dasdasdasd', 'adsasd', 'asdasd', 'asdasdasdasda', 'adasd', 'adadads', 'Hartija', 'Do 50.000', 1, '2018-07-08 15:55:42', '2018-07-08 15:55:42'),
(3, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'adf', 'asdf', 'Automobilska industrija', 'Od 10 mil–50 mil', 5, '2018-09-05 09:13:43', '2018-09-05 09:13:43'),
(4, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Poslovne usluge', 'Od 100 mil–500 mil', 6, '2018-12-19 15:09:07', '2018-12-19 15:09:07'),
(5, 'asdf', 'asdf', 'asdfa', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'Poslovne usluge', 'Od 5 mil–10 mil', 7, '2018-12-23 09:19:40', '2018-12-23 09:19:40'),
(6, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'Poslovne usluge', 'Preko 500 mil', 8, '2018-12-24 16:11:28', '2018-12-24 16:11:28'),
(7, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Poslovne usluge', 'Od 100 mil–500 mil', 9, '2018-12-24 17:18:58', '2018-12-24 17:18:58'),
(8, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Industrija kože', 'Od 10 mil–50 mil', 10, '2018-12-25 09:53:13', '2018-12-25 09:53:13'),
(9, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'Hemijska industrija', 'Od 10 mil–50 mil', 11, '2018-12-25 09:59:35', '2018-12-25 09:59:35'),
(10, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Hemijska industrija', 'Od 50 mil-100 mil', 12, '2018-12-25 10:33:32', '2018-12-25 10:33:32'),
(11, 'asdfasdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Hemijska industrija', 'Od 50 mil-100 mil', 14, '2018-12-25 10:41:58', '2018-12-25 10:41:58'),
(12, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'adfasdf', 'asdfadf', 'asdfasdf', 'Automobilska industrija', 'Od 5 mil–10 mil', 21, '2018-12-26 08:04:49', '2018-12-26 08:04:49'),
(13, 'adfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Poslovne usluge', 'Od 100 mil–500 mil', 23, '2018-12-27 16:29:17', '2018-12-27 16:29:17'),
(14, 'asdfasdf', 'asdfadsf', 'asdfadsf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdff', 'asdfasdf', 'Hemijska industrija', 'Od 10 mil–50 mil', 26, '2018-12-27 18:08:55', '2018-12-27 18:08:55'),
(15, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'sdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Poslovne usluge', 'Od 10 mil–50 mil', 27, '2018-12-27 18:29:51', '2018-12-27 18:29:51'),
(16, 'asdfasdf', 'adfadf', 'asdfasd', 'asdfasdf', 'asdfasdf', 'asdfadsf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Automobilska industrija', 'Od 10 mil–50 mil', 28, '2018-12-27 18:51:13', '2018-12-27 18:51:13'),
(17, 'asdfasddf', 'adfaffd', 'asdfasdf', 'asdfasdf', 'asddfadf', 'asdfasdf', 'asdfadsf', 'asdfadf', 'aasdfadf', 'Automobilska industrija', 'Od 5 mil–10 mil', 30, '2018-12-28 10:53:18', '2018-12-28 10:53:18'),
(18, 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdffg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'Poslovne usluge', 'Od 50 mil-100 mil', 31, '2018-12-30 15:45:54', '2018-12-30 15:45:54'),
(19, 'asdfasdff', 'asdfasdf', 'asdfasdf', 'asdfasdff', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Automobilska industrija', 'Od 5 mil–10 mil', 32, '2018-12-30 16:13:05', '2018-12-30 16:13:05'),
(20, 'asdfasdf', 'asdfasdf', 'asdfadsf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'Metalurgija i obrada metala', 'Od 10 mil–50 mil', 33, '2019-01-02 16:02:41', '2019-01-02 16:02:41'),
(21, 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdf', 'asdfasdf', 'asdfasdf', 'Poslovne usluge', 'Od 5 mil–10 mil', 34, '2019-01-02 16:46:36', '2019-01-02 16:46:36'),
(22, 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'Hemijska industrija', 'Od 5 mil–10 mil', 35, '2019-01-04 14:12:06', '2019-01-04 14:12:06'),
(23, 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'sdfgsdfg', 'Poslovne usluge', 'Od 10 mil–50 mil', 36, '2019-01-04 14:13:05', '2019-01-04 14:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `companyName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pib` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactPerson` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullName`, `companyName`, `mb`, `pib`, `contactPerson`, `website`, `address`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'Dragana Poznan', 'company', '3213213132312', '32132131313131', 'Dragana Poznan', 'www.website.com', 'B.Bajica', 'potestati@gmail.com', 'dragus', '$2y$10$iBq.wS8GqzKYZZ0.RZI5AOeQtw6N8oW5JoOQhajFIXViJ2.60KNku', '92hZi4m1gHYZpP9OQcKe7A0e4EGFXZouFNBQ9YqASpTCma3co6YqnWuExdbs', '2018-06-12 10:07:04', '2018-06-12 10:07:04', 1),
(2, 'Ana Komazec', 'asdfads', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfadsf', 'ana.komazec@gmail.com', 'ana', '$2y$10$/0qwzEsRSmzlzC0iP9wEReXHF//inJdjCYPB7Lk4fwgfn.kE6Rml2', 'HR1s628LUxiF6Py39c4zcBy09Dt7i97qijxoYDKYM8sV25ysymIXK0bWsIQq', '2018-12-19 15:07:40', '2018-12-19 15:07:40', 2),
(3, 'admin', 'adfadsf', 'asdfadf', 'asdfasdf', 'asdfasdf', 'asdfasdf', 'asdfasd', 'admin@admin.com', 'admin', '$2y$10$eylBzjCahPj5N/S8EzCkiOyQS1jLst06MDvJ.hCApt6LSMrXZkTHO', 'zn4MRpGR2HY9YwNY6cJzqPVmYYU9zmus8uvBLLezLfZTwHm3NTCiphELi5dL', '2018-12-27 17:13:53', '2018-12-27 17:13:53', 2),
(4, 'test', 'asdfa', 'asdfa', 'asdf', 'adfadff', 'asdfasdf', 'addfasdf', 'testtest@gmail.com', 'test', '$2y$10$xws1OQX9Ee6CBVwLpssJ5u2CwT4tMzTw2sVilr.92KvEKuWCTF57q', 'WeEEzAnADwVmQahZv5jUIlU4XEtkeCJLsxGi70wRLwcFFiKF7D3FGJJHQRaP', '2018-12-28 10:51:27', '2018-12-28 10:51:27', 2),
(5, 'Mirjana', 'asdfasdf', 'asdfasdf', 'asdffasdf', 'asdffasdf', 'asdfasdf', 'asdfasdff', 'mirjana@gmail.com', 'mirjana', '$2y$10$rf2k/H8sBovtTvGTkaFRKOYTkAEHvvIqueCunDQFw2ADSc6qfjBZ6', '7W9Wad0y7Ajw4POMdo6ElQt2TOGAo4MaerUGHqFVpNM3K6nmyq4fWEbcPfks', '2019-01-02 15:58:42', '2019-01-02 15:58:42', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `available_projects`
--
ALTER TABLE `available_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `available_projects_category_id_foreign` (`category_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upiti`
--
ALTER TABLE `upiti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upiti_project_id_foreign` (`project_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `available_projects`
--
ALTER TABLE `available_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `upiti`
--
ALTER TABLE `upiti`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `available_projects`
--
ALTER TABLE `available_projects`
  ADD CONSTRAINT `available_projects_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `upiti`
--
ALTER TABLE `upiti`
  ADD CONSTRAINT `upiti_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
