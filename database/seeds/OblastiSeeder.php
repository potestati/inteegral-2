<?php

use Illuminate\Database\Seeder;

class oblasti_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('oblasti')->insert([
            'areaName' => "air industry",
            'poslovnaOblast' => "VAZDUSNA INDUSTRIJA",
            'slug' => "air-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//2
        DB::table('oblasti')->insert([
            'areaName' => "car industry",
            'poslovnaOblast' => "AUTO INDUSTRIJA",
            'slug' => "car-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//3
        DB::table('oblasti')->insert([
            'areaName' => "business services",
            'poslovnaOblast' => "POSLOVNE USLUGE",
            'slug' => "business-services",
            'status' => "0",
            'category_id' => "0",
        ]);
//4
        DB::table('oblasti')->insert([
            'areaName' => "chemical industry",
            'poslovnaOblast' => "HEMISKA INDUSTRIJA",
            'slug' => "chemical-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//5
        DB::table('oblasti')->insert([
            'areaName' => "construction industry",
            'poslovnaOblast' => "gradjevinska industrija",
            'slug' => "construction-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//6
        DB::table('oblasti')->insert([
            'areaName' => "electric industry",
            'poslovnaOblast' => "ELEKTRO INDUSTRIJA",
            'slug' => "electric-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//7
        DB::table('oblasti')->insert([
            'areaName' => "agriculture food and severages industry",
            'poslovnaOblast' => "industrija hrane pica i agrikulture",
            'slug' => "agriculture-food-and-severages-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//8
        DB::table('oblasti')->insert([
            'areaName' => "clothing industry",
            'poslovnaOblast' => "industrija odece",
            'slug' => "clothing-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//9
        DB::table('oblasti')->insert([
            'areaName' => "machines and gear",
            'poslovnaOblast' => "mašine i oprema",
            'slug' => "machines-and-gear",
            'status' => "0",
            'category_id' => "0",
        ]);
//10
        DB::table('oblasti')->insert([
            'areaName' => "metallurgy",
            'poslovnaOblast' => "metalurgija",
            'slug' => "metallurgy",
            'status' => "0",
            'category_id' => "0",
        ]);
//11
        DB::table('oblasti')->insert([
            'areaName' => "packaging industry",
            'poslovnaOblast' => "industrija ambalaže",
            'slug' => "packaging-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//12
        DB::table('oblasti')->insert([
            'areaName' => "paper industry",
            'poslovnaOblast' => "industrija papira",
            'slug' => "paper-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
        //13
        DB::table('oblasti')->insert([
            'areaName' => "pharmaceutical industry",
            'poslovnaOblast' => "Farmaceutska industrija",
            'slug' => "pharmaceutical-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//14
        DB::table('oblasti')->insert([
            'areaName' => "leather industry",
            'poslovnaOblast' => "industrija kože",
            'slug' => "leather-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//15
        DB::table('oblasti')->insert([
            'areaName' => "textile industry",
            'poslovnaOblast' => "industrija tekstila",
            'slug' => "textile-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//16
        DB::table('oblasti')->insert([
            'areaName' => "plastic and rubber",
            'poslovnaOblast' => "plastika i guma",
            'slug' => "plastic-and-rubber",
            'status' => "0",
            'category_id' => "0",
        ]);
//17
        DB::table('oblasti')->insert([
            'areaName' => "wood industry",
            'poslovnaOblast' => "drvna industrija",
            'slug' => "wood-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//18
        DB::table('oblasti')->insert([
            'areaName' => "software and it technologies",
            'poslovnaOblast' => "softvera i tehnologija",
            'slug' => "software-and-it-technologies",
            'status' => "0",
            'category_id' => "0",
        ]);
//19
        DB::table('oblasti')->insert([
            'areaName' => "turistic services",
            'poslovnaOblast' => "turisticke usluge",
            'slug' => "turistic-services",
            'status' => "0",
            'category_id' => "0",
        ]);
//20
        DB::table('oblasti')->insert([
            'areaName' => "sports and recreation",
            'poslovnaOblast' => "sport i rekreacija",
            'slug' => "sports-and-recreation",
            'status' => "0",
            'category_id' => "0",
        ]);
//21
        DB::table('oblasti')->insert([
            'areaName' => "loan business",
            'poslovnaOblast' => "kreditni poslovi",
            'slug' => "loan-business",
            'status' => "0",
            'category_id' => "0",
        ]);
//22
        DB::table('oblasti')->insert([
            'areaName' => "free industial zones",
            'poslovnaOblast' => "slobodna industriska zona",
            'slug' => "free-industial-zones",
            'status' => "0",
            'category_id' => "0",
        ]);
//23
        DB::table('oblasti')->insert([
            'areaName' => "energy industry",
            'poslovnaOblast' => "energetska industrija",
            'slug' => "energy-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//24
        DB::table('oblasti')->insert([
            'areaName' => "infrastructure",
            'poslovnaOblast' => "infrastruktura",
            'slug' => "infrastructure",
            'status' => "0",
            'category_id' => "0",
        ]);
        //25
        DB::table('oblasti')->insert([
            'areaName' => "wather industry",
            'poslovnaOblast' => "industrija vode",
            'slug' => "wather-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//26
        DB::table('oblasti')->insert([
            'areaName' => "eco-industry",
            'poslovnaOblast' => "eko industrija",
            'slug' => "eco-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//27
        DB::table('oblasti')->insert([
            'areaName' => "non profitable projects the quality of life",
            'poslovnaOblast' => "neprofitabilni projekti kvalitet zivota",
            'slug' => "non-profitable-projects-the-quality-of-life",
            'status' => "0",
            'category_id' => "0",
        ]);
//28
        DB::table('oblasti')->insert([
            'areaName' => "to 50000 eur",
            'poslovnaOblast' => "do 50000 eur",
            'slug' => "to-50000-eur",
            'status' => "0",
            'category_id' => "0",
        ]);
        //29
        DB::table('oblasti')->insert([
            'areaName' => "from 50001 to 100000 eur",
            'poslovnaOblast' => "od 50001 do 100000 eur",
            'slug' => "from-50001-to-100000-eur",
            'status' => "0",
            'category_id' => "0",
        ]);
//30
        DB::table('oblasti')->insert([
            'areaName' => "from 100001 to 500000 eur",
            'poslovnaOblast' => "od 100001 do 500000 eur",
            'slug' => "from-100001-to-500000-eur",
            'status' => "0",
            'category_id' => "0",
        ]);
//31
        DB::table('oblasti')->insert([
            'areaName' => "from 500001 to 1000000 eur",
            'poslovnaOblast' => "od 500001 do 1000000 eur",
            'slug' => "from-500001-to-1000000-eur",
            'status' => "0",
            'category_id' => "0",
        ]);
//32
        DB::table('oblasti')->insert([
            'areaName' => "air industry",
            'poslovnaOblast' => "od 1 mil do 5 mil",
            'slug' => "air-industry",
            'status' => "0",
            'category_id' => "0",
        ]);
//33
        DB::table('oblasti')->insert([
            'areaName' => "from 5 mil to 10 mil",
            'poslovnaOblast' => "od 5 mil do 10 mil",
            'slug' => "from 5 mil to 10 mil",
            'status' => "0",
            'category_id' => "0",
        ]);
        //34
        DB::table('oblasti')->insert([
            'areaName' => "from 10 mil to 50 mil",
            'poslovnaOblast' => "od 10 mil do 50 mil",
            'slug' => "from-10-mil-to-50-mil",
            'status' => "0",
            'category_id' => "0",
        ]);
//35
        DB::table('oblasti')->insert([
            'areaName' => "from 50 mil to 100 mil",
            'poslovnaOblast' => "od 50 mil do 100 mil",
            'slug' => "from-50-mil-to-100-mil",
            'status' => "0",
            'category_id' => "0",
        ]);
//36
        DB::table('oblasti')->insert([
            'areaName' => "from 100 mil to 500 mil",
            'poslovnaOblast' => "od 100 mil do 500 mil",
            'slug' => "from-100-mil-to-500-mil",
            'status' => "0",
            'category_id' => "0",
        ]);
//37
        DB::table('oblasti')->insert([
            'areaName' => "over 500 mil",
            'poslovnaOblast' => "preko 500 mil",
            'slug' => "over-500-mil",
            'status' => "0",
            'category_id' => "0",
        ]);
    }
}
