<?php

use Illuminate\Database\Seeder;

class categories_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'categoryName' => "MAP of INVESTMENT PROJECTS",
            'imeKategorije' => "MAPA INVESTICIONIH PROJEKATA",
            'categoryDescription' => "",
            'categoryImage' => "red-serbia.png",
            'slug' => "map-of-investment-projects",
            'status' => "1",
        ]);

        DB::table('categories')->insert([
            'categoryName' => "MAP of STRATEGIC PARTNERSHIP",
            'imeKategorije' => "MAPA STRATESKIH PARTNERSTAVA",
            'categoryDescription' => "yellow-serbia.png",
            'categoryImage' => "",
            'slug' => "map-of-strategic-partnership",
            'status' => "0",
        ]);

        DB::table('categories')->insert([
            'categoryName' => "MAP of PRODUCTS AND SERVICES",
            'imeKategorije' => "MAPA PROIZVODA I USLUGA",
            'categoryDescription' => "",
            'categoryImage' => "blue-serbia.png",
            'slug' => "map-of-products-and-services",
            'status' => "0",
        ]);
       
        DB::table('categories')->insert([
            'categoryName' => "MAP of PATENTS",
            'imeKategorije' => "MAPA PATENATA I INOVACIJA",
            'categoryDescription' => "",
            'categoryImage' => "green-serbia.png",
            'slug' => "0",
            'status' => "0",
        ]);
    }
}
