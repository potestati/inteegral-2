<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fullName' => "Dragana Poznan",
            'companyName' => "Company1",
            'mb' => str_random(10),
            'pib' => str_random(10),
            'contactPerson' => str_random(10),
            'website' => str_random(10),
            'email' => "dragana@gmail.com",
            'address' => str_random(10),
            'role_id' => 2,
            'username' => str_random(10),
            'password' => Hash::make('password')
        ]);

        DB::table('users')->insert([
            'fullName' => "Milos Andric",
            'companyName' => "Company2",
            'mb' => str_random(10),
            'pib' => str_random(10),
            'contactPerson' => str_random(10),
            'website' => str_random(10),
            'email' => "milos@gmail.com",
            'address' => str_random(10),
            'role_id' => 1,
            'username' => str_random(10),
            'password' => Hash::make('password')
        ]);
    }
}
